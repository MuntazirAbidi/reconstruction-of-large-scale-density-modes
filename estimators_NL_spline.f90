MODULE HaloTYPE

  TYPE Halo

     INTEGER  :: HN                       ! number of particles in the halo
     REAL(8) :: Hmass                    ! Halo Mass fof=0.2 [10**10 Mo/h]

     REAL(8), DIMENSION(3) :: HPos       ! centre of mass position [Mpc/h]
     REAL(8), DIMENSION(3) :: HVel       ! centre of mass velocity [km/s]

     REAL(8) :: Hrad                     ! radius of halo = mean of last 4 part.
     REAL(8) :: Hradhalf                 ! Half-mass radius
     REAL(8) :: HDisp1D                  ! 1D velocity dispersion [km/s]

     REAL(8), DIMENSION(6) :: HInert     ! Inertia matrix:
     ! I11, I22, I33, I12, I13, I23
  END TYPE Halo

END MODULE HaloTYPE

PROGRAM power
USE halotype
USE eval           ! Frequently used Routines for Correlation and Power Spectrum
USE snapshot       ! Reads in Dark Matter Particles
IMPLICIT NONE


! All that in and out Files
CHARACTER*200 :: Folder
CHARACTER*200 :: InFileBase,PowerFile
CHARACTER*200 :: PsFileBase, PsOutFile,Extension , dmgaussFile
CHARACTER*200 :: datadir,HalFileBase,HalInFile,HaloFileBase,HaloOutFile
CHARACTER*10 :: nodestr,snapstr,ncstr

INTEGER :: idat                ! =1 read data; =0 get header
INTEGER :: iPos                ! =1 read positions; =0 do not.
INTEGER :: iVel                ! =1 read velocities; =0 do not.
INTEGER :: iID                 ! =1 read the ID's; =0 do not.
INTEGER :: iRed                ! =1 no redshift space distortions
                                 ! = (2,3,4) apply distortion in (x-,y-,z-)
TYPE(snapFILES) :: sF

! Switches
INTEGER :: doCorrect						! do CIC correction or not
INTEGER :: doBinnedH
INTEGER :: NodeNumber
INTEGER :: FileNumber
INTEGER :: NHalBin,iMBin,cnt,NHalTot


TYPE(halo) :: HaloFinal
INTEGER, PARAMETER :: NMassBins=5
REAL, DIMENSION(NMassBins+1) :: MBinB
INTEGER, DIMENSION(NMassBins+1) :: NBinB
REAL, DIMENSION(NMassBins) :: MBinC
INTEGER, DIMENSION(NMassBins) :: NHalosBin
REAL(4), DIMENSION(:), ALLOCATABLE :: HalM
REAL(4), DIMENSION(:,:), ALLOCATABLE :: HalPos,HalVel


! Box dimensions
REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltadm,deltag, deltasquare, tidal, shift
REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar, deltadmgauss, deltadmgaussRh,gridlinpower
REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: fieldA, fieldB, gridNLpower

INTEGER, DIMENSION(NkBins) :: kBinCnt
REAL(8), DIMENSION(NkBins) :: kTrueBinC
REAL(8), DIMENSION(NkBins) :: powerdfdf,gridpowerspectrum
REAL(8), DIMENSION(NkBins,NMassBins) :: powerdmdg,powerdgdg
REAL(8), DIMENSION(NkBins,NMassBins) :: powerAgD, powerAsD, powerAtD
REAL(8), DIMENSION(NkBins,NMassBins) :: powerAgAg, powerAsAs, powerAtAt
REAL(8), DIMENSION(NkBins,NMassBins) :: powerAgAs, powerAgAt, powerAsAt
REAL(8), DIMENSION(NkBins,3,3,NMassBins) :: powermatrix

REAL :: time1,time2
INTEGER :: OMP_GET_NUM_THREADS,TID,OMP_GET_THREAD_NUM

INTEGER :: i
REAL(4) :: hradius
REAL(8) :: growth

!growth=0.013134836482296224;
growth=0.01313483;
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CALL cpu_TIME(time1)
! Size of FFT Grid
NCell=256
box=1500.
! Redshift Output
FileNumber=9
! Realization
NodeNumber=1

CALL genbink(0.003d0,0.5d0,'log')

hradius = 10.0
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!				> > > FILENAMES < < <
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

IF (FileNumber<10) THEN
    WRITE(snapstr,'(a2,i1)') '00',FileNumber
ELSE
    WRITE(snapstr,'(a1,i2)') '0',FileNumber
ENDIF

!simulation
IF (NodeNumber<10) THEN
    WRITE(nodestr,'(i1)') NodeNumber
ELSE
    WRITE(nodestr,'(i2)') NodeNumber
ENDIF

CALL getarg(1,nodestr)

IF (Ncell<100) THEN
    WRITE(ncstr,'(i2)') Ncell
ELSEIF (Ncell<1000) THEN
    WRITE(ncstr,'(i3)') Ncell
ELSEIF (Ncell<10000) THEN
    WRITE(ncstr,'(i4)') Ncell
ENDIF

doCorrect=1
doBinnedH=1

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Output File Names
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Extension='NODE'//TRIM(nodestr)//'_'//ASSIGN//'C1_'//TRIM(ncstr)//'_'
Folder='/fast/space/projects/dp002/dc-abid1/reconstruction/Data/check/'

!PsFileBase='power_'
!PsOutFile=trim(Folder)//trim(PsFileBase)//trim(Extension)//trim(snapstr)//'.dat'
!write(*,*) 'Writing Power-Spectrum to ',PsOutFile


!Halo Number Mass Bins
!NbinB=(/20,60,180,540,1620,4860/)

!datadir='/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//trim(nodestr)//'/DATA/reducedGROUPS/'
!HalInFile=trim(datadir)//'GRPS_CMS_wmap7_fid_run'//trim(nodestr)//'_'//trim(snapstr)


! initial Gaussian field
dmgaussFile = '/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//TRIM(nodestr)//'_ZELD/ICs/delta_256.dat'

PowerFile = 'wmap7_camb_matterpower_z0.dat'


!///////////////////////////////////////////////////////////////////////////////
!__________________________________DARK MATTER__________________________________
!///////////////////////////////////////////////////////////////////////////////
WRITE(*,'(a)') '\n\n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
WRITE(*,'(a)') '          > > > Loading initial Gaussian Field < < <'
WRITE(*,'(a)') ' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'


ALLOCATE(deltadmgauss(1:NCell+2,1:NCell,1:NCell))

deltadmgauss = 0.


! loading the initidal gaussian field - this field is already in Fourier transform so no need to do CIC correction.
OPEN(13,file=dmgaussFile,form='unformatted',status='old')
READ(13) deltadmgauss
CLOSE(13)

!///////////////////////////////////////////////////////////////////////////////
!__________________________________DARK MATTER__________________________________
!///////////////////////////////////////////////////////////////////////////////

WRITE(*,'(a)') '\n\n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
WRITE(*,'(a)') '          > > > Loading dark matter < < <'
WRITE(*,'(a)') ' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'



ALLOCATE(deltadm(1:NCell+2,1:NCell,1:NCell))


deltadm=0.

iDat=1; iPos=0; iVel=0; iID=0; iRed=0


! Fiducial Cosmology Final
sf%SNAPDATA = '/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//TRIM(nodestr)//'/DATA/'
sf%SNAPBASE = 'wmap7_fid_run'//TRIM(nodestr)//'_'
sf%SNAPEXT = snapstr
sf%SNAPNFILES = 24


CALL read_snap(sF,iDat,iPos,iVel,iID,iRed,NCell,deltadm)
CALL normalize(deltadm)


CALL fft3d(deltadm,deltadm,'f')


IF (doCorrect==1) THEN
    CALL ciccorrect(deltadm,1.0d0)
ENDIF


ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.
CALL comprod(deltadm,deltadm,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powerdfdf)
DEALLOCATE(deltar)


stop
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! First FFT - Determines FFT strategy
!allocate(deltar(1:NCell+2,1:NCell,1:NCell))
!call fft3d(deltar,deltar,'f')
!deallocate(deltar)
ALLOCATE( deltadmgaussRh(1:NCell+2,1:NCell,1:NCell))
ALLOCATE(gridlinpower(1:NCell+2,1:NCell,1:NCell))
! calculating power spectrum on grid
CALL genPfield(gridlinpower, PowerFile)

ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.0d0
CALL comprod(gridlinpower,gridlinpower,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,gridpowerspectrum)
DEALLOCATE(deltar)


OPEN(23,file='gridlinspowerspectrumsq.dat',form='formatted',status='replace')
DO i=1,NkBins
WRITE(23,'(2es20.10)') kTrueBinC(i),gridpowerspectrum(i)*box**3.0
ENDDO
CLOSE(23)


!gridlinpower = gridlinpower*(growth**2)

DO iMBin=1,5
      ALLOCATE(tidal(1:NCell+2,1:NCell,1:NCell))
      ALLOCATE(shift(1:NCell+2,1:NCell,1:NCell))
      ALLOCATE(deltasquare(1:NCell+2,1:NCell,1:NCell))

      tidal=0.0d0
      shift =0.0d0
      deltasquare=0.0d0

      deltadmgaussRh=0.0d0
      deltadmgaussRh =deltadmgauss
      CALL smdir(deltadmgaussRh,hradius,NCell) ! smoothing of the halo density field

      IF (iMBin==1) THEN
          !========== tidal
          CALL S2recon(deltadmgaussRh,tidal) ! tidal computed from the halo field
		      CALL psigradrecon(deltadmgaussRh,shift) ! shift term from the halo field

      ELSE IF (iMBin==2) THEN
          deltadmgaussRh = deltadmgaussRh/gridlinpower
          CALL S2recon(deltadmgaussRh,tidal) ! tidal computed from the halo field
          CALL psigradrecon(deltadmgaussRh,shift) ! shift term from the halo field

      ELSE IF (iMBin==3) THEN
          !========== tidal
          CALL S2(deltadmgaussRh,1.0d0,tidal) ! tidal computed from the halo field
          CALL fft3d(tidal,tidal,'f')
          CALL psigrad(deltadmgaussRh,deltadmgaussRh,1.0d0,shift) ! shift term from the halo field
          CALL fft3d(shift,shift,'f')

      ELSE IF (iMBin==4) THEN
          CALL S2(deltadmgaussRh,1.0d0,tidal) ! tidal computed from the halo field
          CALL fft3d(tidal,tidal,'f')
          CALL H2reconK2(deltadmgaussRh,shift) ! shift term from the halo field

      ELSE IF (iMBin==5) THEN
         ALLOCATE(fieldA(1:NCell+2,1:NCell,1:NCell))
         ALLOCATE(fieldB(1:NCell+2,1:NCell,1:NCell))
          !CALL S2(deltadmgaussRh,1.0d0,tidal) ! tidal computed from the halo field
          !CALL fft3d(tidal,tidal,'f')
          !CALL H2reconQ2(deltadmgaussRh,shift) ! shift term from the halo field
          fieldA = deltadmgaussRh
          fieldB = deltadmgaussRh/gridlinpower
          CALL psigradrecon2p(fieldA,fieldB,shift)
          CALL S2recon2p(fieldA,fieldB,tidal)

      END IF


		  !========== deltahsquare
      IF (iMBin == 5) THEN
          CALL fft3d(fieldA,fieldA,'e')
          CALL fft3d(fieldB,fieldB,'e')
          deltasquare = fieldA*fieldB  ! square of the halo field
			    CALL fft3d(deltasquare,deltasquare,'f')

          DEALLOCATE(fieldA)
          DEALLOCATE(fieldB)

      ELSE
		      CALL fft3d(deltadmgaussRh,deltadmgaussRh,'e')
			    deltasquare = deltadmgaussRh*deltadmgaussRh  ! square of the halo field
			    CALL fft3d(deltasquare,deltasquare,'f')
          CALL fft3d(deltadmgaussRh,deltadmgaussRh,'f')
      END IF

      !=============  <Ag, delta>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.0d0
			CALL comprod(deltadmgauss,deltasquare,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,1,1,iMBin))
			DEALLOCATE(deltar)
			!=============  <As, delta>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.0d0
			CALL comprod(deltadmgauss,shift,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,1,2,iMBin))
			DEALLOCATE(deltar)
			!=============  <At, delta>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.0d0
			CALL comprod(deltadmgauss,tidal,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,1,3,iMBin))
			DEALLOCATE(deltar)


			!=============  <Ag, Ag>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.0d0
			CALL comprod(deltasquare,deltasquare,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,2,1,iMBin))
			DEALLOCATE(deltar)
			!=============  <As, As>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.0d0
			CALL comprod(shift,shift,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,2,2,iMBin))
			DEALLOCATE(deltar)
			!=============  <At, At>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.0d0
			CALL comprod(tidal,tidal,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,2,3,iMBin))
			DEALLOCATE(deltar)

			!=============  <Ag, As>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.0d0
			CALL comprod(deltasquare,shift,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,3,1,iMBin))
			DEALLOCATE(deltar)
			!=============  <Ag, At>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.0d0
			CALL comprod(deltasquare,tidal,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,3,2,iMBin))
			DEALLOCATE(deltar)
			!=============  <As, At>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.0d0
			CALL comprod(shift,tidal,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,3,3,iMBin))
			DEALLOCATE(deltar)

      !=============  linear power spectrum
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.0d0
			CALL comprod(deltadmgauss,deltadmgauss,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powerdfdf)
			DEALLOCATE(deltar)
			!=============  <As, delta>

      CALL estimatorhdf5export(TRIM(Folder)//'Check_CorrMatrix_kernels_NL'//'_NODE'//TRIM(nodestr)//'_'//TRIM(ncstr)//'_Rh10.h5','Correlations',kbinc, kbincnt,gridpowerspectrum,powermatrix)

      DEALLOCATE(deltasquare)
      DEALLOCATE(tidal)
      DEALLOCATE(shift)

ENDDO

DEALLOCATE(deltadmgaussRh)
DEALLOCATE(gridlinpower)
DEALLOCATE(deltadm)

CALL cpu_TIME(time2)
time2=(time2-time1)
WRITE(*,'(a,f8.2)') 'All done in', time2


END PROGRAM power
