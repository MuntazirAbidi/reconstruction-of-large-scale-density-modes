MODULE HaloTYPE

  TYPE Halo

     INTEGER  :: HN                       ! number of particles in the halo
     REAL(8) :: Hmass                    ! Halo Mass fof=0.2 [10**10 Mo/h]

     REAL(8), DIMENSION(3) :: HPos       ! centre of mass position [Mpc/h]
     REAL(8), DIMENSION(3) :: HVel       ! centre of mass velocity [km/s]

     REAL(8) :: Hrad                     ! radius of halo = mean of last 4 part.
     REAL(8) :: Hradhalf                 ! Half-mass radius
     REAL(8) :: HDisp1D                  ! 1D velocity dispersion [km/s]

     REAL(8), DIMENSION(6) :: HInert     ! Inertia matrix:
     ! I11, I22, I33, I12, I13, I23
  END TYPE Halo

END MODULE HaloTYPE

PROGRAM power
USE halotype
USE eval           ! Frequently used Routines for Correlation and Power Spectrum
USE snapshot       ! Reads in Dark Matter Particles
IMPLICIT NONE


! All that in and out Files
CHARACTER*200 :: Folder
CHARACTER*200 :: InFileBase
CHARACTER*200 :: PsFileBase, PsOutFile,Extension , dmgaussFile
CHARACTER*200 :: datadir,HalFileBase,HalInFile,HaloFileBase,HaloOutFile
CHARACTER*10 :: nodestr,snapstr,ncstr

INTEGER :: idat                ! =1 read data; =0 get header
INTEGER :: iPos                ! =1 read positions; =0 do not.
INTEGER :: iVel                ! =1 read velocities; =0 do not.
INTEGER :: iID                 ! =1 read the ID's; =0 do not.
INTEGER :: iRed                ! =1 no redshift space distortions
                                 ! = (2,3,4) apply distortion in (x-,y-,z-)
TYPE(snapFILES) :: sF

! Switches
INTEGER :: doCorrect						! do CIC correction or not
INTEGER :: doBinnedH
INTEGER :: NodeNumber
INTEGER :: FileNumber
INTEGER :: NHalBin,iMBin,cnt,NHalTot


TYPE(halo) :: HaloFinal
INTEGER, PARAMETER :: NMassBins=5
REAL, DIMENSION(NMassBins+1) :: MBinB
INTEGER, DIMENSION(NMassBins+1) :: NBinB
REAL, DIMENSION(NMassBins) :: MBinC
INTEGER, DIMENSION(NMassBins) :: NHalosBin
REAL(4), DIMENSION(:), ALLOCATABLE :: HalM
REAL(4), DIMENSION(:,:), ALLOCATABLE :: HalPos,HalVel


! Box dimensions
REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltadm,deltag, deltahsquare, tidal, shift
REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar, deltadmgauss

INTEGER, DIMENSION(NkBins) :: kBinCnt
REAL(8), DIMENSION(NkBins) :: kTrueBinC
REAL(8), DIMENSION(NkBins) :: powerdfdf
REAL(8), DIMENSION(NkBins,NMassBins) :: powerdmdg,powerdgdg
REAL(8), DIMENSION(NkBins,NMassBins) :: powerAgD, powerAsD, powerAtD
REAL(8), DIMENSION(NkBins,NMassBins) :: powerAgAg, powerAsAs, powerAtAt
REAL(8), DIMENSION(NkBins,NMassBins) :: powerAgAs, powerAgAt, powerAsAt
REAL(8), DIMENSION(NkBins,3,NMassBins) :: avg_matrix

REAL :: time1,time2
INTEGER :: OMP_GET_NUM_THREADS,TID,OMP_GET_THREAD_NUM

INTEGER :: i
REAL(4) :: hradius




!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CALL CPU_TIME(time1)
! Size of FFT Grid
NCell=256
box=1500.
! Redshift Output
FileNumber=9
! Realization
NodeNumber=1

CALL genbink(0.003d0,0.5d0,'log')

hradius = 0.0
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!				> > > FILENAMES < < <
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

IF (FileNumber<10) THEN
    WRITE(snapstr,'(a2,i1)') '00',FileNumber
ELSE
    WRITE(snapstr,'(a1,i2)') '0',FileNumber
ENDIF

!simulation
IF (NodeNumber<10) THEN
    WRITE(nodestr,'(i1)') NodeNumber
ELSE
    WRITE(nodestr,'(i2)') NodeNumber
ENDIF

CALL getarg(1,nodestr)

IF (Ncell<100) THEN
    WRITE(ncstr,'(i2)') Ncell
ELSEIF (Ncell<1000) THEN
    WRITE(ncstr,'(i3)') Ncell
ELSEIF (Ncell<10000) THEN
    WRITE(ncstr,'(i4)') Ncell
ENDIF

doCorrect=1
doBinnedH=1

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Output File Names
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Extension='NODE'//TRIM(nodestr)//'_'//ASSIGN//'C1_'//TRIM(ncstr)//'_'
Folder='/fast/space/projects/dp002/dc-abid1/reconstruction/Data/average/'

PsFileBase='estimators_avg_'
PsOutFile=TRIM(Folder)//TRIM(PsFileBase)//TRIM(Extension)//TRIM(snapstr)//'.dat'
WRITE(*,*) 'Writing average to ',PsOutFile


!Halo Number Mass Bins
NbinB=(/20,60,180,540,1620,4860/)

datadir='/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//TRIM(nodestr)//'/DATA/reducedGROUPS/'
HalInFile=TRIM(datadir)//'GRPS_CMS_wmap7_fid_run'//TRIM(nodestr)//'_'//TRIM(snapstr)


! initial Gaussian field
dmgaussFile = '/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//TRIM(nodestr)//'_ZELD/ICs/delta_256.dat'


!///////////////////////////////////////////////////////////////////////////////
!__________________________________DARK MATTER__________________________________
!///////////////////////////////////////////////////////////////////////////////
WRITE(*,'(a)') '\n\n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
WRITE(*,'(a)') '          > > > Loading initial Gaussian Field < < <'
WRITE(*,'(a)') ' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'


ALLOCATE(deltadmgauss(1:NCell+2,1:NCell,1:NCell))

deltadmgauss = 0.


! loading the initidal gaussian field - this field is already in Fourier transform so no need to do CIC correction.
OPEN(13,file=dmgaussFile,form='unformatted',status='old')
READ(13) deltadmgauss
CLOSE(13)

!=============  Linear power spectrum : <delta, delta>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(deltadmgauss,deltadmgauss,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powerdfdf)
DEALLOCATE(deltar)



!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


! First FFT - Determines FFT strategy
!allocate(deltar(1:NCell+2,1:NCell,1:NCell))
!call fft3d(deltar,deltar,'f')
!deallocate(deltar)


!///////////////////////////////////////////////////////////////////////////////
!__________________________________DARK MATTER__________________________________
!///////////////////////////////////////////////////////////////////////////////

!write(*,'(a)') '\n\n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
!write(*,'(a)') '          > > > Loading dark matter < < <'
!write(*,'(a)') ' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'



!allocate(deltadm(1:NCell+2,1:NCell,1:NCell))


!deltadm=0.

!iDat=1; iPos=0; iVel=0; iID=0; iRed=0


! Fiducial Cosmology Final
!sf%SNAPDATA = '/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//trim(nodestr)//'/DATA/'
!sf%SNAPBASE = 'wmap7_fid_run'//trim(nodestr)//'_'
!sf%SNAPEXT = snapstr
!sf%SNAPNFILES = 24


!call read_snap(sF,iDat,iPos,iVel,iID,iRed,NCell,deltadm)
!call normalize(deltadm)


!call fft3d(deltadm,deltadm,'f')


!if (doCorrect==1) then
!    call ciccorrect(deltadm,1.0d0)
!endif



!    allocate(deltar(1:NCell+2,1:NCell,1:NCell))
!    deltar=0.
!    call comprod(deltadm,deltadm,deltar)
!    call powerspectrum(deltar,kBinCnt,kTrueBinC,powerdfdf)
!    deallocate(deltar)

! Write Output
!open(20,file=PsOutFile,form='formatted',status='replace')
!    do i=1,NkBins
!	write(20,'(2ES30.10,1I30,1ES30.10)') kbinc(i),ktruebinc(i),kbincnt(i),powerdfdf(i)
!    enddo
CLOSE(20)




!///////////////////////////////////////////////////////////////////////////////
!__________________________________Mass dependent power_________________________
!///////////////////////////////////////////////////////////////////////////////
ALLOCATE(tidal(1:NCell+2,1:NCell,1:NCell))
ALLOCATE(shift(1:NCell+2,1:NCell,1:NCell))
ALLOCATE(deltahsquare(1:NCell+2,1:NCell,1:NCell))

tidal=0
shift =0
deltahsquare=0.

	IF (doBinnedH==1) THEN


	DO iMBin=1,5
		WRITE(*,'(a)') '\n\n ***********************************************'
		WRITE(*,'(a)') '          > > > Halos Mass-Binned < < <'
		WRITE(*,'(a)') ' ***********************************************\n\n'

		OPEN(11,file=TRIM(HalInFile),status='old',form='unformatted')
		READ(11) NHalTot

		PRINT*,'NHalTot',NHalTot
		cnt=0
		DO i=1,NHalTot
			READ(11) HaloFINAL
			IF (HaloFINAL%HN>=NBinB(iMBin) .AND. HaloFINAL%HN<NBinB(iMBin+1)) THEN
					cnt=cnt+1
			ENDIF
		END DO
		CLOSE(11)
		NHalBin=cnt
		PRINT*,'NHalBin',NHalBin

		ALLOCATE(HalM(NHalBin))
		ALLOCATE(HalPos(3,NHalBin))
		ALLOCATE(HalVel(3,NHalBin))



		OPEN(11,file=TRIM(HalInFile),status='old',form='unformatted')
		READ(11) NHalTot
		cnt=0
		DO i=1,NHalTot
			READ(11) HaloFINAL
			IF (HaloFINAL%HN>=NBinB(iMBin) .AND. HaloFINAL%HN<NBinB(iMBin+1)) THEN
			    cnt=cnt+1
			    HalM(cnt)=HaloFINAL%HMass
			    HalPos(:,cnt)=HaloFINAL%HPos(:)
			    HalVel(:,cnt)=HaloFINAL%HVel(:)/SQRT(0.0d0+1.0d0)
			ENDIF
		END DO
		CLOSE(11)

		PRINT*,'Haloes Loaded'

			ALLOCATE(deltag(1:NCell+2,1:NCell,1:NCell))
			deltag=0.d0

			CALL cicmass(NHalBin,HalPos,deltag)
			MBinC(iMBin)=SUM(HalM)/REAL(NHalBin)

			CALL normalize(deltag)


			CALL fft3d(deltag,deltag,'f')

			IF (doCorrect==1) THEN
 				CALL ciccorrect(deltag,1.0d0)
			ENDIF

			CALL smdir(deltag,hradius,NCell) ! smoothing of the halo density field

		        !============= initializing shift and
		        shift = 0.0d0
		        tidal = 0.0d0

		        !========== shift
		        CALL psigrad(deltag,deltag,1.0d0,shift) ! shift term from the halo field
		        CALL fft3d(shift,shift,'f')
		        !========== tidal
		        CALL S2(deltag,1.0d0,tidal) ! tidal computed from the halo field
			      CALL fft3d(tidal,tidal,'f')
		        !========== deltahsquare
		        CALL fft3d(deltag,deltag,'e')
			      deltahsquare = deltag*deltag  ! square of the halo field
			      CALL fft3d(deltahsquare,deltahsquare,'f')


		        CALL fft3d(deltag,deltag,'f')

      !=============  field biining
 			CALL fieldavg(deltahsquare,kBinCnt,kTrueBinC,avg_matrix(:,1,iMBin))
      CALL fieldavg(shift,kBinCnt,kTrueBinC,avg_matrix(:,2,iMBin))
      CALL fieldavg(tidal,kBinCnt,kTrueBinC,avg_matrix(:,3,iMBin))


			! deallocating delta halo field
			DEALLOCATE(deltag)


			DEALLOCATE(HalM)
			DEALLOCATE(HalVel)
			DEALLOCATE(HalPos)

		ENDDO
	ENDIF

        CALL ESTAVGhdf5export(TRIM(Folder)//'Estimators_avg'//'_NODE'//TRIM(nodestr)//'_'//TRIM(ncstr)//'.h5','average',kbinc, kbincnt,powerdfdf, avg_matrix)
        PRINT*,'kbins',NkBins

	!if (doBinnedH==1) then


	!open(20,file=trim(HaloOutFile),form='formatted',status='replace')
 	!do i=1,NkBins
		!write(20,'(2ES30.10,1I30,11ES30.10)') kbinc(i),ktruebinc(i),kbincnt(i),powerdfdf(i), powerdmdg(i,:), powerdgdg(i,:)
   	!enddo
   	!close(20)

 	!endif



DEALLOCATE(deltadmgauss)
DEALLOCATE(deltahsquare)
DEALLOCATE(tidal)
DEALLOCATE(shift)

CALL CPU_TIME(time2)
time2=(time2-time1)
WRITE(*,'(a,f8.2)') 'All done in', time2


END PROGRAM power
