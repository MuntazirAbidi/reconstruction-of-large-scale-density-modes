MODULE eval
  IMPLICIT NONE

  INTEGER :: NCell
  INTEGER, PARAMETER :: NkBins=60
  INTEGER, PARAMETER :: NmuBins=20
  INTEGER, PARAMETER :: NrBins=60
  REAL(8), DIMENSION(nkbins) :: kbinc
  REAL(8), DIMENSION(nkbins+1) :: kbinb
  REAL(8), DIMENSION(nmubins) :: mubinc
  REAL(8), DIMENSION(nmubins+1) :: mubinb
  REAL(8), PARAMETER :: massUNIT= 1.0d10
  CHARACTER*3, PARAMETER :: ASSIGN='CIC'
  CHARACTER*3,PARAMETER :: ffttype='est'
  REAL(8) :: box
  REAL :: rhobar
  INTEGER,PARAMETER :: chunk=10
  INTEGER,PARAMETER :: nthr=16
  INTEGER,PARAMETER :: nthrfftw=10
  INTEGER ,PARAMETER :: doSN=0
  INTEGER ,PARAMETER :: doRed=0
  INTEGER, PARAMETER :: NAngBins=1
  REAL(8), PARAMETER :: pi=3.14159265d0
  INTEGER, PARAMETER :: ithresh=512/2

CONTAINS

  !///////////////////////////////////////////////////////////////////////////////
  !____________________________Generate Delta__________________________________
  !///////////////////////////////////////////////////////////////////////////////

  subroutine genPfield(grid,powerfile)
  use nrmods
  implicit none

  real(8), dimension(:,:,:) :: grid
  character*200 :: powerfile
  integer, parameter :: nkp=807
  real(4), dimension(nkp) :: kp,p,dp
  real(4) :: x1,x2
  real(8) :: a1,a2
  integer :: i,j,l
  real(4) :: sig

  open(21,file=trim(powerfile),form='formatted',status='old')
  !print*,'I am here', nkp
      do i=1,nkp
  	read(21,*) kp(i),p(i)
  	!print*, kp(i),p(i)
      enddo
  close(21)
  call spline(kp,p,3.0e30,3.0e30,dp)

  grid=0.0d0
  x1=0.0
  x2=0.0
  print*,'NCell', NCell
  do i=1,Ncell+2,2
      do j=1,Ncell
  	do l=1,Ncell
  	    grid(i,j,l)=splint(kp,p,dp,real(kmag(i,j,l)))
  	    grid(i+1,j,l)=splint(kp,p,dp,real(kmag(i,j,l)))
!print*, real(kmag(i,j,l)),splint(kp,p,dp,real(kmag(i,j,l)))
  	enddo
      enddo
  enddo

end subroutine genPfield

subroutine genPfieldspline(grid,power,kp)
use nrmods
implicit none

real(8), dimension(:,:,:) :: grid
integer, parameter :: nkp=60
real(4), dimension(nkp) :: kp,dp
real(4), dimension(nkp) :: power
real(4) :: x1,x2
real(8) :: a1,a2
integer :: i,j,l
real(4) :: sig


call spline(kp,power,3.0e30,3.0e30,dp)

grid=0.0d0
x1=0.0
x2=0.0
print*,'NCell', NCell
do i=1,Ncell+2,2
    do j=1,Ncell
  do l=1,Ncell
      grid(i,j,l)=splint(kp,power,dp,real(kmag(i,j,l)))
      grid(i+1,j,l)=splint(kp,power,dp,real(kmag(i,j,l)))
  enddo
    enddo
enddo

end subroutine genPfieldspline

  !###############################################################################
  function kmag(i,j,l)
      implicit none
      real(8) :: kmag
      integer :: i,j,l
      kmag=sqrt(i2k((i+1)/2)**2.0d0+i2k(j)**2.0d0+i2k(l)**2.0d0)
  end function kmag

  !###############################################################################
  function i2k(i)
      implicit none
      real(8) :: i2k
      integer :: i

      if (i < Ncell/2 + 2) then
  	i2k=2.0d0*pi/box*dble(i-1)
      else
  	i2k=-2.0d0*pi/box*dble(-i + (Ncell + 1)+1-1)
      endif
  end function i2k


  !///////////////////////////////////////////////////////////////////////////////
  !______________________________________GENBINK__________________________________
  !///////////////////////////////////////////////////////////////////////////////
  SUBROUTINE genbink(kmin,kmax,bintype)
    IMPLICIT NONE
    INTEGER :: i
    REAL(8) :: kmin,kmax,dklogbin,dklin
    CHARACTER*3 :: bintype

    IF (bintype=='log') THEN
       dkLogBin=LOG10(kmax/kmin)/REAL(NkBins)
       kBinB(1)=kmin
       DO i=2,NkBins+1
          kbinB(i)=LOG10(kmin)+(i-1)*dkLogBin
          kbinB(i)=10**kbinB(i)
          kbinC(i-1)=LOG10(kmin)+(REAL(i-1)-0.5)*dkLogBin
          kbinC(i-1)=10**kbinC(i-1)
       ENDDO

    ELSE IF (bintype=='lin') THEN

       dklin=(kmax-kmin)/REAL(NkBins)
       kBinB(1)=kmin
       DO i=2,NkBins+1
          kbinB(i)=kmin+(i-1)*dklin
          kbinC(i-1)=kmin+(REAL(i-1)-0.5)*dklin
       ENDDO

    ELSE
       PRINT*, 'Not a valid binning'
       STOP
    ENDIF

  END SUBROUTINE genbink
  !///////////////////////////////////////////////////////////////////////////////
  !______________________________________GENBINK__________________________________
  !///////////////////////////////////////////////////////////////////////////////
  SUBROUTINE genbinmu()
    IMPLICIT NONE
    INTEGER :: i
    REAL(8) :: dmu
    CHARACTER*3 :: bintype

    dmu=2.0d0/REAL(NmuBins)
    muBinB(1)=-1.0d0
    DO i=2,NmuBins+1
       mubinB(i)=-1.0d0+(i-1)*dmu
       mubinC(i-1)=-1.0d0+(REAL(i-1)-0.5)*dmu
    ENDDO


  END SUBROUTINE genbinmu
  !///////////////////////////////////////////////////////////////////////////////
  !______________________________________GENBINK__________________________________
  !///////////////////////////////////////////////////////////////////////////////
  SUBROUTINE genbinr(kmin,kmax,bintype)
    IMPLICIT NONE
    INTEGER :: i
    REAL(8) :: kmin,kmax,dklogbin,dklin
    CHARACTER*3 :: bintype

    IF (bintype=='log') THEN
       dkLogBin=LOG10(kmax/kmin)/REAL(NkBins)
       kBinB(1)=kmin
       DO i=2,NkBins+1
          kbinB(i)=LOG10(kmin)+(i-1)*dkLogBin
          kbinB(i)=10**kbinB(i)
          kbinC(i-1)=LOG10(kmin)+(REAL(i-1)-0.5)*dkLogBin
          kbinC(i-1)=10**kbinC(i-1)
       ENDDO

    ELSE IF (bintype=='lin') THEN

       dklin=(kmax-kmin)/REAL(NkBins)
       kBinB(1)=kmin
       DO i=2,NkBins+1
          kbinB(i)=kmin+(i-1)*dklin
          kbinC(i-1)=kmin+(REAL(i-1)-0.5)*dklin
       ENDDO

    ELSE
       PRINT*, 'Not a valid binning'
       STOP
    ENDIF

  END SUBROUTINE genbinr



  !///////////////////////////////////////////////////////////////////////////////
  !_______________________   Smoothing____________________________________________
  !///////////////////////////////////////////////////////////////////////////////

  SUBROUTINE smdir(grid,rad,NCell)
    IMPLICIT NONE
    REAL(8), DIMENSION(NCell+2,NCell,NCell) :: grid
    INTEGER :: i,j,k,NCell
    REAL(4) :: rad,kr
    REAL(4) :: kx,ky,kz
    !grid=0.
    DO k=1,NCell
       IF (k .LT. NCell/2+1) THEN
          kz=k-1
       ELSE
          kz=k-1-NCell
       ENDIF
       DO j=1,NCell
          IF (j .LT. NCell/2+1) THEN
             ky=j-1
          ELSE
             ky=j-1-NCell
          ENDIF
          DO i=1,NCell+2,2
             kx=(i-1)/2
             kr=SQRT(kx**2.0+ky**2.0+kz**2.0)*2.0*pi/box
             IF (kr>0.0) THEN
                grid(i,j,k)=grid(i,j,k)*EXP(-(kr*rad)**2.0/2.0d0)
                grid(i+1,j,k)=grid(i+1,j,k)*EXP(-(kr*rad)**2.0/2.0d0)
             ENDIF
          ENDDO
       ENDDO
    ENDDO
  END SUBROUTINE smdir





  !///////////////////////////////////////////////////////////////////////////////
  !______________________________________COMPROD__________________________________
  !///////////////////////////////////////////////////////////////////////////////

  SUBROUTINE comprod(in1,in2,out)
    ! computes the complex product of two grids
    IMPLICIT NONE
    REAL(8), DIMENSION(:,:,:) :: in1,in2,out
    INTEGER :: i,j,k
    REAL :: time1,time2
    CALL CPU_TIME(time1)
    out=0.
    !$OMP PARALLEL DO SHARED(out,in1,in2) PRIVATE(k,j,i) NUM_THREADS(NTHR) SCHEDULE(DYNAMIC,CHUNK)
    DO j=1,NCell
       DO k=1,NCell
          DO i=1,NCell+2,2
             out(i,j,k)=in1(i,j,k)*in2(i,j,k)+in1(i+1,j,k)*in2(i+1,j,k)
             out(i+1,j,k)=-in1(i,j,k)*in2(i+1,j,k)+in1(i+1,j,k)*in2(i,j,k)
          ENDDO
       ENDDO
    ENDDO
    !$OMP END PARALLEL DO

    CALL CPU_TIME(time2)
    time2=(time2-time1)
    WRITE(*,'(a,f8.2)') ' Comprod done in', time2
    RETURN
  END SUBROUTINE comprod

  !///////////////////////////////////////////////////////////////////////////////
  !______________________________________CICCORRECT______________________________
  !///////////////////////////////////////////////////////////////////////////////

  SUBROUTINE ciccorrect(GridArr,Ntot)
    ! Corrects for the CIC filter function
    IMPLICIT NONE

    REAL(8), DIMENSION(:,:,:) :: GridArr
    REAL(8) :: NTot,ncr,W,Wx,Wy,Wz,Wsx,Wsy,Wsz
    INTEGER :: i,j,k,nc,l,m,n
    REAL(8) :: kx,ky,kz
    REAL(8) :: factor,ws
    REAL(8) :: expo
    REAL(8), PARAMETER :: pi=3.141592653589793d0

    SELECT CASE(ASSIGN)
    CASE('NGP')
       expo=1.d0
    CASE('CIC')
       expo=2.d0
    CASE default
       expo=0.d0
       WRITE( *, * ) 'Unspecified assignment Method'
    END SELECT


    ncr=DBLE(NCell)
    nc=NCell

    factor=2.*pi/box

    DO i=1,nc+2,2
       IF (i .GT. 1) THEN
          l=(i-1)/2
          Wx=(SIN(pi*DBLE(l)/ncr)/(pi*DBLE(l)/ncr))
          Wsx=SIN(pi*DBLE(l)/ncr)
       ELSE
          l=1
          Wx=1.d0
          Wsx=1.0d0
       ENDIF
       DO j=1,nc
          IF (j .GT. nc/2+1) THEN
             m=j-nc-1
             Wy=(SIN(pi*DBLE(m)/ncr)/(pi*DBLE(m)/ncr))
             Wsy=SIN(pi*DBLE(m)/ncr)
          ELSEIF (j .GT. 1) THEN
             m=j-1
             Wy=(SIN(pi*DBLE(m)/ncr)/(pi*DBLE(m)/ncr))
             Wsy=SIN(pi*DBLE(m)/ncr)
          ELSE
             m=1
             Wy=1.d0
             Wsy=1.0d0
          ENDIF
          DO k=1,nc
             IF (k .GT. nc/2+1) THEN
                n=k-nc-1
                Wz=(SIN(pi*DBLE(n)/ncr)/(pi*DBLE(n)/ncr))
                Wsz=SIN(pi*DBLE(n)/ncr)
             ELSEIF (k .GT. 1) THEN
                n=k-1
                Wz=(SIN(pi*DBLE(n)/ncr)/(pi*DBLE(n)/ncr))
                Wsz=SIN(pi*DBLE(n)/ncr)
             ELSE
                n=1
                Wz=1.d0
                Wsz=1.0d0
             ENDIF
             W=Wx*Wy*Wz
             WS=(1.0d0-2.d0/3.d0*Wsx**2.0)*(1.0d0-2.d0/3.d0*Wsy**2.0)*(1.0d0-2.d0/3.d0*Wsz**2.0)
             IF (W .NE. 0.) THEN
                IF(doSN==1) THEN
                   GridArr(i,j,k)=(GridArr(i,j,k)-1.0d0/Ntot*Ws)/W**expo
                ELSE
                   GridArr(i,j,k)=GridArr(i,j,k)/W**expo
                ENDIF
                GridArr(i+1,j,k)=GridArr(i+1,j,k)/W**expo
             END IF
             IF (ABS(W) < 0.0001) THEN
                PRINT *, W,l,m,n
             ENDIF
          ENDDO
       ENDDO
    ENDDO
    WRITE( *, * ) 'Finished assignment correction'
  END SUBROUTINE ciccorrect



  !///////////////////////////////////////////////////////////////////////////////
  !______________________________________NORMALIZATION_____________________________
  !///////////////////////////////////////////////////////////////////////////////
  SUBROUTINE normalize(GridArr)
    IMPLICIT NONE

    REAL(8), DIMENSION(:,:,:) :: GridArr
    REAL(8) :: NTot
    REAL(8) :: ncr
    INTEGER :: i,j,k,nc
    REAL(8) :: factor
    REAL :: time1,time2
    INTEGER, PARAMETER :: chunkloc=4
    CALL CPU_TIME(time1)

    ncr=DBLE(NCell)
    nc=NCell
    NTot=0.d0


    !$OMP PARALLEL DO SHARED(GridArr,nc) PRIVATE(k,j,i)  NUM_THREADS(chunk) SCHEDULE(DYNAMIC,CHUNKloc) &
    !$OMP REDUCTION(+:NTot)
    DO j=1,nc
       DO k=1,nc
          DO i=1,nc
             NTot=NTot+GridArr(i,j,k)
          ENDDO
       ENDDO
    ENDDO

    !$OMP END PARALLEL DO



    WRITE(*,'(a,F20.1)') ' Total Number assigned: ',Ntot

    factor=1.d0/NTot*ncr*ncr*(ncr)
    !$OMP PARALLEL DO SHARED(GridArr,NTot,factor,nc) PRIVATE(k,j,i)  NUM_THREADS(chunk) SCHEDULE(DYNAMIC,CHUNKloc)
    DO j=1,nc
       DO k=1,nc
          DO i=1,nc
             GridArr(i,j,k)=GridArr(i,j,k)*factor-1.0d0
          ENDDO
       ENDDO
    ENDDO
    !$OMP END PARALLEL DO


    NTot=0.d0

    !$OMP PARALLEL DO SHARED(GridArr,nc) PRIVATE(k,j,i)  NUM_THREADS(chunk) SCHEDULE(DYNAMIC,CHUNKloc) &
    !$OMP REDUCTION(+:NTot)
    DO j=1,nc
       DO k=1,nc
          DO i=1,nc
             NTot=NTot+GridArr(i,j,k)
          ENDDO
       ENDDO
    ENDDO
    !$OMP END PARALLEL DO




    WRITE(*,'(a,ES8.1)') ' Check if sum=0: ',NTot
    CALL CPU_TIME(time2)
    time2=(time2-time1)
    WRITE(*,'(a,f8.2)') ' Summation done in', time2
    RETURN

  END SUBROUTINE normalize

  !///////////////////////////////////////////////////////////////////////////////
  !______________________________________CIC MASS__________________________________
  !///////////////////////////////////////////////////////////////////////////////
  SUBROUTINE cicmass(NPart,PosArr,GridArr)
    IMPLICIT NONE
    INTEGER :: NC, NPart
    REAL(4), DIMENSION(3,NPart) :: PosArr
    REAL(8), DIMENSION(:,:,:) :: GridArr
    REAL(8) :: ncr
    REAL(8) :: factor
    INTEGER :: i
    INTEGER :: i1,i2,j1,j2,k1,k2
    REAL(8) :: x,y,z,dx1,dx2,dy1,dy2,dz1,dz2
    REAL :: time1,time2

    CALL CPU_TIME(time1)
    nc=ncell
    ncr=DBLE(ncell)
    factor=ncr/box

    SELECT CASE(ASSIGN)

    CASE('NGP')

       PRINT *,'NGP'
       DO i=1,NPart


          x=MOD(PosArr(1,i)*factor,ncr)
          y=MOD(PosArr(2,i)*factor,ncr)
          z=MOD(PosArr(3,i)*factor,ncr)

          i1=FLOOR(x)+1
          j1=FLOOR(y)+1
          k1=FLOOR(z)+1

          GridArr(i1,j1,k1)=GridArr(i1,j1,k1)+1.0

       ENDDO

    CASE('CIC')
       PRINT *,'CIC'

       DO i=1,NPart
          x=MOD(PosArr(1,i)*factor+ncr,ncr)
          y=MOD(PosArr(2,i)*factor+ncr,ncr)
          z=MOD(PosArr(3,i)*factor+ncr,ncr)

          i1=FLOOR(x)+1
          i2=MOD(i1,nc)+1
          dx1=REAL(i1)-x
          dx2=1.0-dx1

          j1=FLOOR(y)+1
          j2=MOD(j1,nc)+1
          dy1=REAL(j1)-y
          dy2=1.0-dy1

          k1=FLOOR(z)+1
          k2=MOD(k1,nc)+1
          dz1=REAL(k1)-z
          dz2=1.0-dz1

          GridArr(i1,j1,k1)=GridArr(i1,j1,k1)+dx1*dy1*dz1
          GridArr(i2,j1,k1)=GridArr(i2,j1,k1)+dx2*dy1*dz1
          GridArr(i1,j2,k1)=GridArr(i1,j2,k1)+dx1*dy2*dz1
          GridArr(i2,j2,k1)=GridArr(i2,j2,k1)+dx2*dy2*dz1
          GridArr(i1,j1,k2)=GridArr(i1,j1,k2)+dx1*dy1*dz2
          GridArr(i2,j1,k2)=GridArr(i2,j1,k2)+dx2*dy1*dz2
          GridArr(i1,j2,k2)=GridArr(i1,j2,k2)+dx1*dy2*dz2
          GridArr(i2,j2,k2)=GridArr(i2,j2,k2)+dx2*dy2*dz2
       ENDDO

    CASE default
       WRITE( *, * ) 'Unspecified assignment Method'
    END SELECT



    CALL CPU_TIME(time2)
    time2=(time2-time1)
    WRITE(*,'(a,f8.2)') ' Assignment done in', time2
    RETURN

  END SUBROUTINE cicmass


  !///////////////////////////////////////////////////////////////////////////////
  !									SIJGEN
  !///////////////////////////////////////////////////////////////////////////////
  SUBROUTINE sijgen(delta,sijvec,ind)
    IMPLICIT NONE
    REAL(8), DIMENSION(:,:,:) :: delta
    REAL(8), DIMENSION(:,:,:) :: sijvec
    INTEGER :: i,j,k,ind
    REAL(8) :: kr,kx,ky,kz
    REAL(8) :: time1,time2
    REAL(8), DIMENSION(6) :: prodvec,subvec
    INTEGER :: OMP_GET_NUM_THREADS,nthr,TID,OMP_GET_THREAD_NUM
    REAL(8) :: OMP_GET_WTIME


    time1=OMP_GET_WTIME()

    !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(k,j,i,kz,ky,kx,kr,prodvec,subvec) NUM_THREADS(chunk)
    DO k=1,ncell
       IF (k .LT. ncell/2+1) THEN
          kz=k-1
       ELSE
          kz=(k-1-ncell)
       ENDIF
       DO j=1,ncell
          IF (j .LT. ncell/2+1) THEN
             ky=j-1
          ELSE
             ky=(j-1-ncell)
          ENDIF
          DO i=1,ncell+2,2
             kx=(i-1)/2
             kr=SQRT(kx**2.0d0+ky**2.0d0+kz**2.0d0)
             IF (kr .GT. 0.0d0) THEN
                prodvec=(/kx*kx,ky*ky,kz*kz,kx*ky,kx*kz,ky*kz/)
                subvec=(/1.0d0,1.0d0,1.0d0,0.0d0,0.0d0,0.0d0/)
                sijvec(i,j,k)=(prodvec(ind)/kr**2.0d0-0.0d0/3.0d0*subvec(ind))*delta(i,j,k);
                sijvec(i+1,j,k)=(prodvec(ind)/kr**2.0d0-0.0d0/3.0d0*subvec(ind))*delta(i+1,j,k);
             ENDIF
          ENDDO
       ENDDO
    ENDDO
    !$OMP END PARALLEL DO

    time2=OMP_GET_WTIME()
    time2=(time2-time1)
    WRITE(*,*) 'sij Calculated in',time2

    RETURN
  END SUBROUTINE sijgen


  !///////////////////////////////////////////////////////////////////////////////
  !______________________________________GRADGEN = GRADIENTGENERATOR__________________________________
  !///////////////////////////////////////////////////////////////////////////////

  SUBROUTINE gradgen(delta,gradvec,ind)
    IMPLICIT NONE

    REAL(8), DIMENSION(:,:,:) :: delta, gradvec
    INTEGER :: i,j,k, ind
    REAL(8) :: kr,kx,ky,kz
    REAL :: time1, time2
    REAL(8), DIMENSION(3) :: prodvec

    CALL CPU_TIME(time1)

    !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(k,j,i,kz,kx,ky,kr,prodvec) NUM_THREADS(chunk)

    DO k =1, ncell
       IF (k .LT. ncell/2 +1) THEN
          kz = k - 1
       ELSE
          kz = k - ncell -1
       ENDIF

       DO j = 1, ncell
          IF (j .LT. ncell/2 + 1) THEN
             ky = j - 1
          ELSE
             ky = j - ncell - 1
          ENDIF

          DO i=1, ncell + 2, 2
             kx = (i-1)/2
             kr = SQRT(kx**2.0d0 + ky**2.0d0 + kz**2.0d0)
             IF (kr .GT. 0.0d0) THEN
                prodvec = (/kx,ky,kz/)*2.0d0*pi/box
                gradvec(i+1,j,k) = -prodvec(ind)*delta(i,j,k)
                gradvec(i,j,k) = prodvec(ind)*delta(i+1,j,k)
             ENDIF
          ENDDO
       ENDDO
    ENDDO

    !OMP END PARALLEL DO

    RETURN

  END SUBROUTINE gradgen


  !///////////////////////////////////////////////////////////////////////////////
  !_____________________________________psigen = _genpsik__________________________________
  !///////////////////////////////////////////////////////////////////////////////

  SUBROUTINE psigen(delta, psivec, ind)

    IMPLICIT NONE

    REAL(8), DIMENSION(:,:,:) :: delta, psivec
    INTEGER :: i,j,k, ind
    REAL(8) :: kr,kx,ky,kz
    REAL :: time1, time2
    REAL(8), DIMENSION(3) :: prodvec

    CALL CPU_TIME(time1)

    !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(k,j,i,kz,kx,ky,kr,prodvec) NUM_THREADS(chunk)

    DO k=1, ncell
       IF (k .LT. ncell/2+1) THEN
          kz = k - 1
       ELSE
          kz = k-ncell-1
       ENDIF

       DO j = 1, ncell
          IF (j .LT. ncell/2 + 1) THEN
             ky = j - 1
          ELSE
             ky = j-ncell-1
          ENDIF

          DO i = 1, ncell + 2, 2
             kx = (i-1)/2
             kr = (2.0d0*pi/box)*SQRT(kx**2.0d0 + ky**2.0d0 + kz**2.0d0)
             IF (kr .GT. 0.0d0) THEN
                prodvec=(/kx,ky,kz/)*2.0*pi/box
                psivec(i+1,j,k) = prodvec(ind)/kr**2.0d0*delta(i,j,k);
                psivec(i,j,k)=-prodvec(ind)/kr**2.0d0*delta(i+1,j,k);
             ENDIF

          ENDDO
       ENDDO
    ENDDO


    !OMP END PARALLEL DO

    RETURN
  END SUBROUTINE psigen


  !///////////////////////////////////////////////////////////////////////////////
  !______________________________________PSIGRAD = Psi\grad \delta________________
  !///////////////////////////////////////////////////////////////////////////////

  ! this subroutine computes the shift term
  SUBROUTINE psigrad(psifield,gradfield,prefdelta,addfielddelta)
    IMPLICIT NONE
    REAL(8), DIMENSION(:,:,:) :: psifield,gradfield,addfielddelta
    REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar,deltar2
    INTEGER :: i
    REAL(8) :: prefdelta

    DO i=1,3
       PRINT *, i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
       ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))
       deltar=0.0d0
       deltar2=0.0d0
       CALL gradgen(gradfield,deltar,i)   ! Fourier space
       CALL psigen(psifield, deltar2,i)   ! Fourier space
       CALL fft3d(deltar,deltar,'e')      ! inverse Fourier transform of gradient field
       CALL fft3d(deltar2,deltar2,'e')    ! inverse Fourier transform of psi field

       deltar(Ncell+1:Ncell+2,:,:)=0.0d0   ! configuration space
       deltar2(Ncell+1:Ncell+2,:,:)=0.0d0  ! configuration space

       addfielddelta=addfielddelta+prefdelta*deltar*deltar2
       !addfieldtheta=addfieldtheta+preftheta*deltar*deltar2

       DEALLOCATE(deltar)
       DEALLOCATE(deltar2)
    ENDDO

  END SUBROUTINE psigrad


  !///////////////////////////////////////////////////////////////////////////////
  !									KK=S^2
  !///////////////////////////////////////////////////////////////////////////////
  SUBROUTINE S2(fielda,prefdelta,addfielddelta)
    IMPLICIT NONE
    REAL(8), DIMENSION(:,:,:) :: fielda,addfielddelta
    REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar,deltar2
    INTEGER :: i
    REAL(8) :: prefdelta

    DO i=1,3
       PRINT*,i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))

       deltar=0.0d0

       CALL sijgen(fielda,deltar,i)

       CALL fft3d(deltar,deltar,'e')

       deltar(Ncell+1:Ncell+2,:,:)=0.0d0

       addfielddelta=addfielddelta+prefdelta*deltar*deltar

       DEALLOCATE(deltar)
    ENDDO

    DO i=4,6
       PRINT*,i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))

       deltar=0.0d0

       CALL sijgen(fielda,deltar,i)

       CALL fft3d(deltar,deltar,'e')

       deltar(Ncell+1:Ncell+2,:,:)=0.0d0

       addfielddelta=addfielddelta+2.0*prefdelta*deltar*deltar

       DEALLOCATE(deltar)
    ENDDO

  END SUBROUTINE S2



  !///////////////////////////////////////////////////////////////////////////////
  !________________________________Bining function_____________________________
  !///////////////////////////////////////////////////////////////////////////////

  SUBROUTINE fieldavg(delta,binCnt,binK,binP)
    IMPLICIT NONE
    REAL(8), DIMENSION(:,:,:) :: delta
    INTEGER :: w
    INTEGER :: i,j,k
    REAL(8) :: kr,kx,ky,kz,pow
    REAL :: time1,time2
    REAL(8), DIMENSION(nkbins) :: binP,binK
    INTEGER, DIMENSION(nkbins) :: binCnt

    CALL CPU_TIME(time1)

    binCnt=0
    binP=0.d0
    binK=0.d0


    !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(k,j,i,kz,ky,kx,kr,pow,w) NUM_THREADS(nthr) &
    !$OMP REDUCTION(+:binCnt,binP,binK)
    DO k=1,ncell
       IF (k .LT. ncell/2+1) THEN
          kz=k-1
       ELSE
          kz=k-1-ncell
       ENDIF
       DO j=1,ncell
          IF (j .LT. ncell/2+1) THEN
             ky=j-1
          ELSE
             ky=j-1-ncell
          ENDIF
          DO i=1,ncell+2,2
             kx=(i-1)/2
             kr=SQRT(kx**2+ky**2+kz**2)*2.0d0*pi/box
             IF (kr .NE. 0) THEN

                pow=delta(i,j,k)
                DO w=1,nkbins
                   IF ((kr>kbinb(w)) .AND.(kr<=kbinb(w+1))) THEN
                      binCnt(w)=binCnt(w)+1
                      binP(w)=binP(w)+pow
                      binK(w)=binK(w)+kr
                   ENDIF
                ENDDO
             ENDIF
          ENDDO
       ENDDO
    ENDDO
    !$OMP END PARALLEL DO


    DO w=1,nkbins
       IF (binCnt(w) .GT. 0) THEN
          binP(w)=1.0d0/box**3.0d0*binP(w)/REAL(binCnt(w))
          binK(w)=binK(w)/REAL(binCnt(w))
       ENDIF
    ENDDO



    CALL CPU_TIME(time2)
    time2=(time2-time1)
    WRITE(*,'(a,f8.2)') ' Averaging calculated in', time2
    RETURN
  END SUBROUTINE fieldavg



  !///////////////////////////////////////////////////////////////////////////////
  !______________________________________Powerspectrum_____________________________
  !///////////////////////////////////////////////////////////////////////////////

  SUBROUTINE powerspectrum(delta,binCnt,binK,binP)
    IMPLICIT NONE
    REAL(8), DIMENSION(:,:,:) :: delta
    INTEGER :: w
    INTEGER :: i,j,k
    REAL(8) :: kr,kx,ky,kz,pow
    REAL :: time1,time2
    REAL(8), DIMENSION(nkbins) :: binP,binK
    INTEGER, DIMENSION(nkbins) :: binCnt

    CALL CPU_TIME(time1)

    binCnt=0
    binP=0.d0
    binK=0.d0


    !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(k,j,i,kz,ky,kx,kr,pow,w) NUM_THREADS(nthr) &
    !$OMP REDUCTION(+:binCnt,binP,binK)
    DO k=1,ncell
       IF (k .LT. ncell/2+1) THEN
          kz=k-1
       ELSE
          kz=k-1-ncell
       ENDIF
       DO j=1,ncell
          IF (j .LT. ncell/2+1) THEN
             ky=j-1
          ELSE
             ky=j-1-ncell
          ENDIF
          DO i=1,ncell+2,2
             kx=(i-1)/2
             kr=SQRT(kx**2+ky**2+kz**2)*2.0d0*pi/box
             IF (kr .NE. 0) THEN
                pow=delta(i,j,k)
                DO w=1,nkbins
                   IF ((kr>kbinb(w)) .AND.(kr<=kbinb(w+1))) THEN ! inside the shell
                      binCnt(w)=binCnt(w)+1
                      binP(w)=binP(w)+pow
                      binK(w)=binK(w)+kr
                   ENDIF
                ENDDO
             ENDIF
          ENDDO
       ENDDO
    ENDDO
    !$OMP END PARALLEL DO


    DO w=1,nkbins
       IF (binCnt(w) .GT. 0) THEN
          binP(w)=1.0d0/box**3.0d0*binP(w)/REAL(binCnt(w))
          binK(w)=binK(w)/REAL(binCnt(w))
       ENDIF
    ENDDO

    CALL CPU_TIME(time2)
    time2=(time2-time1)
    WRITE(*,'(a,f8.2)') ' Power spectrum calculated in', time2
    RETURN
  END SUBROUTINE powerspectrum

  !///////////////////////////////////////////////////////////////////////////////
  !				Reconstruction: estimators
  !///////////////////////////////////////////////////////////////////////////////

  SUBROUTINE S2recon(field,addfielddelta)
    IMPLICIT NONE
    REAL(8), DIMENSION(:,:,:) :: field ,addfielddelta
    REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar,deltar2
    INTEGER :: i
    REAL(8) :: prefdelta

    DO i=1,3
       PRINT*,i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
       ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))

       deltar=0.0d0

       CALL sijgen(field,deltar,i)
       CALL fft3d(field,field,'e')
       CALL fft3d(deltar,deltar,'e')

       deltar=deltar*field
       deltar(Ncell+1:Ncell+2,:,:)=0.0d0

       CALL fft3d(field,field,'f')
       CALL fft3d(deltar,deltar,'f')
       CALL sijgen(deltar,deltar2,i)

       addfielddelta=addfielddelta+deltar2

       DEALLOCATE(deltar,deltar2)
    ENDDO

    DO i=4,6
       PRINT*,i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
       ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))

       deltar=0.0d0

       CALL sijgen(field,deltar,i)
       CALL fft3d(field,field,'e')
       CALL fft3d(deltar,deltar,'e')

       deltar=deltar*field
       deltar(Ncell+1:Ncell+2,:,:)=0.0d0

       CALL fft3d(field,field,'f')
       CALL fft3d(deltar,deltar,'f')
       CALL sijgen(deltar,deltar2,i)

       addfielddelta=addfielddelta+2.0*deltar2

       DEALLOCATE(deltar,deltar2)
    ENDDO

  END SUBROUTINE S2recon

  SUBROUTINE S2recon2p(fielda,fieldb,addfielddelta)
    !fielda=deltag/Pfull, fieldb=deltag*Plin/Pfull
    IMPLICIT NONE
    REAL(8), DIMENSION(:,:,:) :: fielda,fieldb ,addfielddelta
    REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar,deltar2
    INTEGER :: i
    REAL(8) :: prefdelta

    DO i=1,3
       PRINT*,i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
       ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))

       deltar=0.0d0

       CALL sijgen(fielda,deltar,i)
       CALL fft3d(fieldb,fieldb,'e')
       CALL fft3d(deltar,deltar,'e')


       deltar=deltar*fieldb
       deltar(Ncell+1:Ncell+2,:,:)=0.0d0
       CALL fft3d(fieldb,fieldb,'f')
       CALL fft3d(deltar,deltar,'f')
       CALL sijgen(deltar,deltar2,i)


       addfielddelta=addfielddelta+deltar2

       DEALLOCATE(deltar,deltar2)
    ENDDO

    DO i=4,6
       PRINT*,i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
       ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))

       deltar=0.0d0

       CALL sijgen(fielda,deltar,i)
       CALL fft3d(fieldb,fieldb,'e')
       CALL fft3d(deltar,deltar,'e')

       deltar=deltar*fieldb
       deltar(Ncell+1:Ncell+2,:,:)=0.0d0
       CALL fft3d(fieldb,fieldb,'f')
       CALL fft3d(deltar,deltar,'f')
       CALL sijgen(deltar,deltar2,i)

       addfielddelta=addfielddelta+2.0*deltar2

       DEALLOCATE(deltar,deltar2)
    ENDDO

  END SUBROUTINE S2recon2p

 SUBROUTINE psigradrecon(field,addfielddelta)
    IMPLICIT NONE
    REAL(8), DIMENSION(:,:,:) :: field,addfielddelta
    REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar,deltar2
    INTEGER :: i


    DO i=1,3
       PRINT *, i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
       ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))
       deltar=0.0d0
       deltar2=0.0d0
       CALL gradgen(field,deltar,i)   ! Fourier space
   ! Fourier space
       CALL fft3d(field,field,'e')      ! inverse Fourier transform of gradient field
       CALL fft3d(deltar,deltar,'e')    ! inverse Fourier transform of the linear density field
       deltar=deltar*field              ! multiplication in real space
       deltar(Ncell+1:Ncell+2,:,:)=0.0d0
       CALL fft3d(field,field,'f')      ! density field in Fourier space
       CALL fft3d(deltar,deltar,'f')    ! Fourier space, multplication of the gradient field and lineare field
       CALL psigen(deltar, deltar2,i)

       addfielddelta=addfielddelta+deltar2


       DEALLOCATE(deltar)
       DEALLOCATE(deltar2)


       PRINT *, i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
       ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))
       deltar=0.0d0
       deltar2=0.0d0
       CALL psigen(field,deltar,i)   ! Using the linear field in Fourier space

       CALL fft3d(field,field,'e')      ! inverse Fourier transform of gradient field
       CALL fft3d(deltar,deltar,'e')    ! inverse Fourier transform of psi field
       deltar=deltar*field
       deltar(Ncell+1:Ncell+2,:,:)=0.0d0   ! configuration space
       CALL fft3d(field,field,'f')
       CALL fft3d(deltar,deltar,'f')
       CALL gradgen(deltar, deltar2,i)

       addfielddelta=addfielddelta+deltar2

       DEALLOCATE(deltar)
       DEALLOCATE(deltar2)

    ENDDO


  END SUBROUTINE psigradrecon
!==================================================================================================
!
!==================================================================================================
  SUBROUTINE H2reconK2(field,addfielddelta)   ! Part of the shift term which has k^2 in denominator
     IMPLICIT NONE
     REAL(8), DIMENSION(:,:,:) :: field,addfielddelta
     REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar,deltar2
     INTEGER :: i
     DO i=1,3
        PRINT *, i
        ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
        ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))
        deltar=0.0d0
        deltar2=0.0d0
        CALL gradgen(field,deltar,i)   ! Fourier space
        CALL fft3d(field,field,'e')      ! inverse Fourier transform of gradient field
        CALL fft3d(deltar,deltar,'e')    ! inverse Fourier transform of the linear density field
        deltar=deltar*field              ! multiplication in real space
        deltar(Ncell+1:Ncell+2,:,:)=0.0d0
        CALL fft3d(field,field,'f')      ! density field in Fourier space
        CALL fft3d(deltar,deltar,'f')    ! Fourier space, multplication of the gradient field and lineare field
        CALL psigen(deltar, deltar2,i)

        addfielddelta=addfielddelta+deltar2

        DEALLOCATE(deltar)
        DEALLOCATE(deltar2)

     ENDDO
   END SUBROUTINE H2reconK2


   SUBROUTINE H2reconQ2(field,addfielddelta) ! Part of the shift term which has q^2 in denominator
      IMPLICIT NONE
      REAL(8), DIMENSION(:,:,:) :: field,addfielddelta
      REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar,deltar2
      INTEGER :: i
      DO i=1,3
        PRINT *, i
        ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
        ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))
        deltar=0.0d0
        deltar2=0.0d0
        CALL psigen(field,deltar,i)   ! Using the linear field in Fourier space

        CALL fft3d(field,field,'e')      ! inverse Fourier transform of gradient field
        CALL fft3d(deltar,deltar,'e')    ! inverse Fourier transform of psi field
        deltar=deltar*field

        deltar(Ncell+1:Ncell+2,:,:)=0.0d0
        CALL fft3d(field,field,'f')
        CALL fft3d(deltar,deltar,'f')
        CALL gradgen(deltar, deltar2,i)

        addfielddelta=addfielddelta+deltar2

         DEALLOCATE(deltar)
         DEALLOCATE(deltar2)

      ENDDO
    END SUBROUTINE H2reconQ2

!==================================================================================================
!==================================================================================================


 SUBROUTINE psigradrecon2p(fielda,fieldb,addfielddelta)
    IMPLICIT NONE
    REAL(8), DIMENSION(:,:,:) :: fielda,fieldb,addfielddelta
    REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar,deltar2
    INTEGER :: i


    DO i=1,3
       PRINT *, i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
       ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))
       deltar=0.0d0
       deltar2=0.0d0
       CALL gradgen(fielda,deltar,i)   ! Fourier space
   ! Fourier space
       CALL fft3d(fieldb,fieldb,'e')      ! inverse Fourier transform of gradient field
       CALL fft3d(deltar,deltar,'e')    ! inverse Fourier transform of psi field
       deltar=deltar*fieldb
       deltar(Ncell+1:Ncell+2,:,:)=0.0d0   ! configuration space
       CALL fft3d(fieldb,fieldb,'f')
       CALL fft3d(deltar,deltar,'f')
       CALL psigen(deltar, deltar2,i)

       addfielddelta=addfielddelta+deltar2


       DEALLOCATE(deltar)
       DEALLOCATE(deltar2)


       PRINT *, i
       ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
       ALLOCATE(deltar2(1:NCell+2,1:NCell,1:NCell))
       deltar=0.0d0
       deltar2=0.0d0
       CALL psigen(fielda,deltar,i)   ! Fourier space
   ! Fourier space
       CALL fft3d(fieldb,fieldb,'e')      ! inverse Fourier transform of gradient field
       CALL fft3d(deltar,deltar,'e')    ! inverse Fourier transform of psi field
       deltar=deltar*fieldb
       deltar(Ncell+1:Ncell+2,:,:)=0.0d0   ! configuration space
       CALL fft3d(fieldb,fieldb,'f')
       CALL fft3d(deltar,deltar,'f')
       CALL gradgen(deltar, deltar2,i)

       addfielddelta=addfielddelta+deltar2


       DEALLOCATE(deltar)
       DEALLOCATE(deltar2)


    ENDDO

  END SUBROUTINE psigradrecon2p
  !///////////////////////////////////////////////////////////////////////////////
  !				Bispectrum mmh kkk
  !///////////////////////////////////////////////////////////////////////////////

  SUBROUTINE bispectrum_threefield(deltaa,deltab,deltac,dosym,imax,binCnt,binB,binK)
    IMPLICIT NONE
    INTEGER :: imax,nc
    REAL(8), DIMENSION(:,:,:) :: deltaa,deltab,deltac
    INTEGER :: w1,w2,w3
    INTEGER :: i,i1,i2,i3,j1,j2,j3,k1,k2,k3
    REAL(8) :: kr,kr1,kr2,kr3,kx1,kx2,kx3,ky1,ky2,ky3,kz1,kz2,kz3,pow
    REAL :: time1,time2
    REAL(8), DIMENSION(:,:,:) :: binB
    REAL(8), DIMENSION(:,:,:,:) :: binK
    INTEGER, DIMENSION(:,:,:) :: binCnt
    !integer :: OMP_GET_NUM_THREADS,nthr,TID,OMP_GET_THREAD_NUM
    REAL(8) :: cyfac
    COMPLEX(8) :: c1,c2,c3
    LOGICAL :: dosym

    PRINT*,'Bispectrum Code Started'

    CALL CPU_TIME(time1)
    nc=ncell



    binB=0.0d0
    binCnt=0
    binK=0.0d0
    PRINT*,imax
    PRINT*,'Starting Loops'
    !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(k1,k2,k3,j1,j2,j3,i1,i2,i3,kz1,kz2,kz3,ky1,ky2,ky3,kx1,kx2,kx3,kr1,kr2,kr3,pow,w1,w2,w3,cyfac,c1,c2,c3) NUM_THREADS(nthr) &
    !$OMP REDUCTION(+:binCnt,binB,binK)
    DO k1=1,nc
       !print*,'k1',k1
       !nthr=OMP_GET_NUM_THREADS()
       !TID = OMP_GET_THREAD_NUM()
       !print *,'Threads ',nthr,'ID ',tid

       IF (k1 .LT. imax) THEN
          kz1=k1-1
       ELSE IF(k1 .GT. nc-imax) THEN
          kz1=k1-1-nc
       ELSE
          CYCLE
       ENDIF
       DO j1=1,nc
          !print*,'j1',j1
          IF (j1 .LT. imax) THEN
             ky1=j1-1
          ELSE IF (j1 .GT. nc-imax) THEN
             ky1=j1-1-nc
          ELSE
             CYCLE
          ENDIF
          DO i1=1,2*imax,2
             kx1=(i1-1)/2
             DO k2=1,nc
                IF (k2 .LT. imax) THEN
                   kz2=k2-1
                ELSE IF(k2 .GT. nc-imax) THEN
                   kz2=k2-1-nc
                ELSE
                   CYCLE
                ENDIF
                DO j2=1,nc
                   !print*,'j2',j2
                   IF (j2 .LT. imax) THEN
                      ky2=j2-1
                   ELSE IF (j2 .LT. nc-imax) THEN
                      ky2=j2-1-nc
                   ELSE
                      CYCLE
                   ENDIF
                   DO i2=1,2*imax,2
                      kx2=(i2-1)/2

                      IF (SQRT((kx1+kx2)**2.0+(ky1+ky2)**2.0+(kz1+kz2)**2.0)<imax) THEN

                         kx3=-kx1-kx2
                         ky3=-ky1-ky2
                         kz3=-kz1-kz2

                         IF (kx3<0) THEN
                            i3=-2*kx3+1
                            ky3=-ky3
                            kz3=-kz3
                            cyfac=-1.0d0
                         ELSE
                            i3=2*kx3+1
                            cyfac=1.0d0
                         ENDIF
                         IF (ky3<0) THEN
                            j3=ky3+nc+1
                         ELSE
                            j3=ky3+1
                         ENDIF
                         IF (kz3<0) THEN
                            k3=kz3+nc+1
                         ELSE
                            k3=kz3+1
                         END IF



                         c1=CMPLX(deltaa(i1,j1,k1),deltaa(i1+1,j1,k1))
                         c2=CMPLX(deltab(i2,j2,k2),deltab(i2+1,j2,k2))
                         c3=CMPLX(deltac(i3,j3,k3),cyfac*deltac(i3+1,j3,k3))

                         pow=REAL(c1*c2*c3)

                         IF (dosym .EQV. .TRUE.) THEN
                            c1=CMPLX(deltab(i1,j1,k1),deltab(i1+1,j1,k1))
                            c2=CMPLX(deltaa(i2,j2,k2),deltaa(i2+1,j2,k2))
                            c3=CMPLX(deltac(i3,j3,k3),cyfac*deltac(i3+1,j3,k3))
                            pow=pow+REAL(c1*c2*c3)

                            c1=CMPLX(deltaa(i1,j1,k1),deltaa(i1+1,j1,k1))
                            c2=CMPLX(deltac(i2,j2,k2),deltac(i2+1,j2,k2))
                            c3=CMPLX(deltab(i3,j3,k3),cyfac*deltab(i3+1,j3,k3))
                            pow=pow+REAL(c1*c2*c3)

                            c1=CMPLX(deltac(i1,j1,k1),deltac(i1+1,j1,k1))
                            c2=CMPLX(deltab(i2,j2,k2),deltab(i2+1,j2,k2))
                            c3=CMPLX(deltaa(i3,j3,k3),cyfac*deltaa(i3+1,j3,k3))
                            pow=pow+REAL(c1*c2*c3)

                            c1=CMPLX(deltac(i1,j1,k1),deltac(i1+1,j1,k1))
                            c2=CMPLX(deltaa(i2,j2,k2),deltaa(i2+1,j2,k2))
                            c3=CMPLX(deltab(i3,j3,k3),cyfac*deltab(i3+1,j3,k3))
                            pow=pow+REAL(c1*c2*c3)


                            c1=CMPLX(deltab(i1,j1,k1),deltab(i1+1,j1,k1))
                            c2=CMPLX(deltac(i2,j2,k2),deltac(i2+1,j2,k2))
                            c3=CMPLX(deltaa(i3,j3,k3),cyfac*deltaa(i3+1,j3,k3))
                            pow=pow+REAL(c1*c2*c3)


                            pow=pow/6.0d0
                         ENDIF





                         kr1=SQRT(kx1**2.0+ky1**2.0+kz1**2.0)*2.0d0*pi/box
                         kr2=SQRT(kx2**2.0+ky2**2.0+kz2**2.0)*2.0d0*pi/box
                         kr3=SQRT(kx3**2.0+ky3**2.0+kz3**2.0)*2.0d0*pi/box


                         IF (kr1 > 0.0 .AND. kr2 > 0.0 .AND. kr3 > 0.0) THEN
                            DO w1=1,Nkbins
                               DO w2=1,Nkbins
                                  DO w3=1,Nkbins
                                     IF ((kr1>=kbinb(w1)) .AND.(kr1<kbinb(w1+1)) .AND. (kr2>=kbinb(w2)) .AND.(kr2<kbinb(w2+1)) .AND. (kr3>=kbinb(w3)) .AND.(kr3<kbinb(w3+1))) THEN
                                        !print*,kr1,kr2,kr3
                                        binCnt(w1,w2,w3)=binCnt(w1,w2,w3)+1
                                        binB(w1,w2,w3)=binB(w1,w2,w3)+pow
                                        binK(w1,w2,w3,1)=binK(w1,w2,w3,1)+kr1
                                        binK(w1,w2,w3,2)=binK(w1,w2,w3,2)+kr2
                                        binK(w1,w2,w3,3)=binK(w1,w2,w3,3)+kr3
                                     ENDIF
                                  ENDDO
                               ENDDO
                            ENDDO
                         ENDIF

                      ENDIF
                   ENDDO

                ENDDO
             ENDDO
          ENDDO
       ENDDO
    ENDDO
    !$OMP END PARALLEL DO




    DO w1=1,Nkbins
       DO w2=1,Nkbins
          DO w3=1,Nkbins
             IF (binCnt(w1,w2,w3) .GT. 0) THEN

                binB(w1,w2,w3)=1.0d0/box**3.0*binB(w1,w2,w3)/REAL(binCnt(w1,w2,w3))
                binK(w1,w2,w3,:)=binK(w1,w2,w3,:)/REAL(binCnt(w1,w2,w3))
                !print*,w1,w2,w3,binCnt(w1,w2,w3),binK(w1,w2,w3,:)
             ENDIF
          ENDDO
       ENDDO
    ENDDO
    CALL CPU_TIME(time2)
    time2=(time2-time1)
    WRITE(*,'(a,f8.2)') ' Bispectrum calculated in', time2
    RETURN
  END SUBROUTINE bispectrum_threefield

  SUBROUTINE bispectrum_mu_threefield(deltaa,deltab,deltac,dosym,imax,binCnt,binB,binK)
    IMPLICIT NONE
    INTEGER :: imax,nc
    REAL(8), DIMENSION(:,:,:) :: deltaa,deltab,deltac
    INTEGER :: w1,w2,w3
    INTEGER :: i,i1,i2,i3,j1,j2,j3,k1,k2,k3,x2,x3,mu
    REAL(8) :: kr,kr1,kr2,kr3,kx1,kx2,kx3,ky1,ky2,ky3,kz1,kz2,kz3,pow
    REAL :: time1,time2
    REAL(8), DIMENSION(:,:,:) :: binB
    REAL(8), DIMENSION(:,:,:,:) :: binK
    INTEGER, DIMENSION(:,:,:) :: binCnt
    !integer :: OMP_GET_NUM_THREADS,nthr,TID,OMP_GET_THREAD_NUM
    REAL(8) :: cyfac
    COMPLEX(8) :: c1,c2,c3
    LOGICAL :: dosym

    PRINT*,'Bispectrum Code Started'

    CALL CPU_TIME(time1)
    nc=ncell



    binB=0.0d0
    binCnt=0
    binK=0.0d0
    PRINT*,imax
    PRINT*,'Starting Loops'
    !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(x2,x3,mu,k1,k2,k3,j1,j2,j3,i1,i2,i3,kz1,kz2,kz3,ky1,ky2,ky3,kx1,kx2,kx3,kr1,kr2,kr3,pow,w1,w2,w3,cyfac,c1,c2,c3) NUM_THREADS(nthr) &
    !$OMP REDUCTION(+:binCnt,binB,binK)
    DO k1=1,nc
       !print*,'k1',k1
       !nthr=OMP_GET_NUM_THREADS()
       !TID = OMP_GET_THREAD_NUM()
       !print *,'Threads ',nthr,'ID ',tid

       IF (k1 .LT. imax) THEN
          kz1=k1-1
       ELSE IF(k1 .GT. nc-imax) THEN
          kz1=k1-1-nc
       ELSE
          CYCLE
       ENDIF
       DO j1=1,nc
          !print*,'j1',j1
          IF (j1 .LT. imax) THEN
             ky1=j1-1
          ELSE IF (j1 .GT. nc-imax) THEN
             ky1=j1-1-nc
          ELSE
             CYCLE
          ENDIF
          DO i1=1,2*imax,2
             kx1=(i1-1)/2
             DO k2=1,nc
                IF (k2 .LT. imax) THEN
                   kz2=k2-1
                ELSE IF(k2 .GT. nc-imax) THEN
                   kz2=k2-1-nc
                ELSE
                   CYCLE
                ENDIF
                DO j2=1,nc
                   !print*,'j2',j2
                   IF (j2 .LT. imax) THEN
                      ky2=j2-1
                   ELSE IF (j2 .LT. nc-imax) THEN
                      ky2=j2-1-nc
                   ELSE
                      CYCLE
                   ENDIF
                   DO i2=1,2*imax,2
                      kx2=(i2-1)/2

                      IF (SQRT((kx1+kx2)**2.0+(ky1+ky2)**2.0+(kz1+kz2)**2.0)<imax) THEN

                         kx3=-kx1-kx2
                         ky3=-ky1-ky2
                         kz3=-kz1-kz2

                         IF (kx3<0) THEN
                            i3=-2*kx3+1
                            ky3=-ky3
                            kz3=-kz3
                            cyfac=-1.0d0
                         ELSE
                            i3=2*kx3+1
                            cyfac=1.0d0
                         ENDIF
                         IF (ky3<0) THEN
                            j3=ky3+nc+1
                         ELSE
                            j3=ky3+1
                         ENDIF
                         IF (kz3<0) THEN
                            k3=kz3+nc+1
                         ELSE
                            k3=kz3+1
                         END IF



                         c1=CMPLX(deltaa(i1,j1,k1),deltaa(i1+1,j1,k1))
                         c2=CMPLX(deltab(i2,j2,k2),deltab(i2+1,j2,k2))
                         c3=CMPLX(deltac(i3,j3,k3),cyfac*deltac(i3+1,j3,k3))

                         pow=REAL(c1*c2*c3)

                         IF (dosym .EQV. .TRUE.) THEN
                            c1=CMPLX(deltab(i1,j1,k1),deltab(i1+1,j1,k1))
                            c2=CMPLX(deltaa(i2,j2,k2),deltaa(i2+1,j2,k2))
                            c3=CMPLX(deltac(i3,j3,k3),cyfac*deltac(i3+1,j3,k3))
                            pow=pow+REAL(c1*c2*c3)

                            c1=CMPLX(deltaa(i1,j1,k1),deltaa(i1+1,j1,k1))
                            c2=CMPLX(deltac(i2,j2,k2),deltac(i2+1,j2,k2))
                            c3=CMPLX(deltab(i3,j3,k3),cyfac*deltab(i3+1,j3,k3))
                            pow=pow+REAL(c1*c2*c3)

                            c1=CMPLX(deltac(i1,j1,k1),deltac(i1+1,j1,k1))
                            c2=CMPLX(deltab(i2,j2,k2),deltab(i2+1,j2,k2))
                            c3=CMPLX(deltaa(i3,j3,k3),cyfac*deltaa(i3+1,j3,k3))
                            pow=pow+REAL(c1*c2*c3)

                            c1=CMPLX(deltac(i1,j1,k1),deltac(i1+1,j1,k1))
                            c2=CMPLX(deltaa(i2,j2,k2),deltaa(i2+1,j2,k2))
                            c3=CMPLX(deltab(i3,j3,k3),cyfac*deltab(i3+1,j3,k3))
                            pow=pow+REAL(c1*c2*c3)


                            c1=CMPLX(deltab(i1,j1,k1),deltab(i1+1,j1,k1))
                            c2=CMPLX(deltac(i2,j2,k2),deltac(i2+1,j2,k2))
                            c3=CMPLX(deltaa(i3,j3,k3),cyfac*deltaa(i3+1,j3,k3))
                            pow=pow+REAL(c1*c2*c3)


                            pow=pow/6.0d0
                         ENDIF





                         kr1=SQRT(kx1**2.0+ky1**2.0+kz1**2.0)*2.0d0*pi/box
                         kr2=SQRT(kx2**2.0+ky2**2.0+kz2**2.0)*2.0d0*pi/box
                         kr3=SQRT(kx3**2.0+ky3**2.0+kz3**2.0)*2.0d0*pi/box


                         IF (kr1 > 0.0 .AND. kr2 > 0.0 .AND. kr3 > 0.0) THEN
                            x2=kr2/kr1;
                            x3=kr3/kr1;
                            mu=(x3**2.0-1.0-x2**2.0)/(2.0*x2)
                            DO w1=1,Nkbins
                               DO w2=1,Nkbins
                                  DO w3=1,Nmubins
                                     IF ((kr1>=kbinb(w1)) .AND.(kr1<kbinb(w1+1)) .AND. (kr2>=kbinb(w2)) .AND.(kr2<kbinb(w2+1)) .AND. (mu>=MuBinB(w3)) .AND.(mu<=MuBinB(w3+1))) THEN
                                        !print*,kr1,kr2,kr3
                                        binCnt(w1,w2,w3)=binCnt(w1,w2,w3)+1
                                        binB(w1,w2,w3)=binB(w1,w2,w3)+pow
                                        binK(w1,w2,w3,1)=binK(w1,w2,w3,1)+kr1
                                        binK(w1,w2,w3,2)=binK(w1,w2,w3,2)+kr2
                                        binK(w1,w2,w3,3)=binK(w1,w2,w3,3)+kr3
                                     ENDIF
                                  ENDDO
                               ENDDO
                            ENDDO
                         ENDIF

                      ENDIF
                   ENDDO

                ENDDO
             ENDDO
          ENDDO
       ENDDO
    ENDDO
    !$OMP END PARALLEL DO




    DO w1=1,Nkbins
       DO w2=1,Nkbins
          DO w3=1,Nmubins
             IF (binCnt(w1,w2,w3) .GT. 0) THEN

                binB(w1,w2,w3)=1.0d0/box**3.0*binB(w1,w2,w3)/REAL(binCnt(w1,w2,w3))
                binK(w1,w2,w3,:)=binK(w1,w2,w3,:)/REAL(binCnt(w1,w2,w3))
                !print*,w1,w2,w3,binCnt(w1,w2,w3),binK(w1,w2,w3,:)
             ENDIF
          ENDDO
       ENDDO
    ENDDO
    CALL CPU_TIME(time2)
    time2=(time2-time1)
    WRITE(*,'(a,f8.2)') ' Bispectrum calculated in', time2
    RETURN
  END SUBROUTINE bispectrum_mu_threefield

  !///////////////////////////////////////////////////////////////////////////////
  !				Bispectrum mmh Mudependent
  !///////////////////////////////////////////////////////////////////////////////
  SUBROUTINE bispectrum_fft_equi(delta,radcnt,radcont)
    IMPLICIT NONE
    REAL(8) :: ncr
    INTEGER :: nc
    REAL(8), DIMENSION(:,:,:) :: delta
    INTEGER :: w1,w2,w3
    INTEGER :: i,i1,i2,i3,j1,j2,j3,k1,k2,k3
    REAL(8) :: kr,kr1,kr2,kr3,kx1,kx2,kx3,ky1,ky2,ky3,kz1,kz2,kz3,pow,x2,x3,mu
    REAL :: time1,time2

    REAL(8), DIMENSION(NkBins) :: RadCont,RadContd,RadVar
    REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltaa,deltab,deltac,deltaad,deltabd,deltacd
    INTEGER, DIMENSION(NkBins) :: RadCnt
    INTEGER :: OMP_GET_NUM_THREADS,nthr,TID,OMP_GET_THREAD_NUM
    REAL(8) :: sumsqpower
    REAL(8) :: cyfac
    INTEGER, PARAMETER :: nmin=1



    PRINT*, 'start bispectrum'


    CALL CPU_TIME(time1)
    nc=ncell
    ncr=ncell


    RadCnt=0
    RadCont=0.
    RadContd=0.
    RadVar=0.

    sumsqpower=0.0d0





    DO w1=1,NkBins
       ALLOCATE(deltaa(Nc+2,Nc,Nc))
       deltaa=0.0d0
       ALLOCATE(deltaad(Nc+2,Nc,Nc))
       deltaad=0.0d0

       DO k1=nmin,nc
          !print*,'k1',k1
          !nthr=OMP_GET_NUM_THREADS()
          !TID = OMP_GET_THREAD_NUM()
          !print *,'Threads ',nthr,'ID ',tid


          IF (k1 .LT. ithresh) THEN
             kz1=k1-1
          ELSE IF (k1 .GT. nc-ithresh) THEN
             kz1=k1-1-nc
          ELSE
             CYCLE
          ENDIF
          DO j1=nmin,nc
             !print*,'j1',j1
             IF (j1 .LT. ithresh) THEN
                ky1=j1-1
             ELSE IF (j1 .GT. nc-ithresh) THEN
                ky1=j1-1-nc
             ELSE
                CYCLE
             ENDIF
             DO i1=1,ncell+2,2
                kx1=(i1-1)/2
                kr1=SQRT(kx1**2.0+ky1**2.0+kz1**2.0)*2.0*pi/box
                IF ((kr1>kBinB(w1)) .AND.(kr1<=kBinB(w1+1))) THEN
                   deltaa(i1,j1,k1)=delta(i1,j1,k1)
                   deltaa(i1+1,j1,k1)=delta(i1+1,j1,k1)
                   deltaad(i1,j1,k1)=1.0d0
                   deltaad(i1+1,j1,k1)=0.0d0
                ENDIF
             ENDDO
          ENDDO
       ENDDO
       CALL fft3d(deltaa,deltaa,'e')
       CALL fft3d(deltaad,deltaad,'e')



       PRINT*,'Done ffts'
       !$OMP PARALLEL DO SHARED(nc) PRIVATE(k1,j1,i1)  NUM_THREADS(nthr) SCHEDULE(DYNAMIC,chunk) &
       !$OMP REDUCTION(+:RadCont,RadContD)
       DO j1=1,nc
          DO k1=1,nc
             DO i1=1,nc
                RadCont(w1)=RadCont(w1)+deltaa(i1,j1,k1)**3.0
                RadContD(w1)=RadContD(w1)+deltaad(i1,j1,k1)**3.0
             ENDDO
          ENDDO
       ENDDO
       !$OMP END PARALLEL DO

       IF (RadContD(w1)>0.0d0) THEN
          RadCont(w1)=RadCont(w1)/RadContD(w1)
       ELSE
          RadCont(w1)=0.0d0
       ENDIF


       PRINT*,'Done sum'

       !enddo
       DEALLOCATE(deltaa)
       DEALLOCATE(deltaad)
    ENDDO


    CALL CPU_TIME(time2)
    time2=(time2-time1)
    WRITE(*,'(a,f8.2)') ' Bispectrum calculated in', time2
    RETURN
  END SUBROUTINE bispectrum_fft_equi


  !///////////////////////////////////////////////////////////////////////////////
  !				Bispectrum mmh Mudependent
  !///////////////////////////////////////////////////////////////////////////////
  SUBROUTINE bispectrum_fft_kkk(delta,radcnt,radcont)
    IMPLICIT NONE
    REAL(8) :: ncr
    INTEGER :: nc
    REAL(8), DIMENSION(:,:,:) :: delta
    INTEGER :: w1,w2,w3
    INTEGER :: i,i1,i2,i3,j1,j2,j3,k1,k2,k3
    REAL(8) :: kr,kr1,kr2,kr3,kx1,kx2,kx3,ky1,ky2,ky3,kz1,kz2,kz3,pow,x2,x3,mu
    REAL :: time1,time2

    REAL(8), DIMENSION(NkBins,NkBins,NkBins) :: RadCont,RadContd,RadVar
    REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltaa,deltab,deltac,deltaad,deltabd,deltacd
    INTEGER, DIMENSION(NkBins,NkBins,NkBins) :: RadCnt
    INTEGER :: OMP_GET_NUM_THREADS,nthr,TID,OMP_GET_THREAD_NUM
    REAL(8) :: sumsqpower
    REAL(8) :: cyfac
    INTEGER, PARAMETER :: nmin=1



    PRINT*, 'start bispectrum'


    CALL CPU_TIME(time1)
    nc=ncell
    ncr=ncell




    RadCnt=0
    RadCont=0.
    RadContd=0.
    RadVar=0.

    sumsqpower=0.0d0


    DO w1=1,NkBins
       PRINT*,'w1',w1
       ALLOCATE(deltaa(Nc+2,Nc,Nc))
       deltaa=0.0d0
       ALLOCATE(deltaad(Nc+2,Nc,Nc))
       deltaad=0.0d0

       DO k1=nmin,nc
          !print*,'k1',k1
          !nthr=OMP_GET_NUM_THREADS()
          !TID = OMP_GET_THREAD_NUM()
          !print *,'Threads ',nthr,'ID ',tid


          IF (k1 .LT. ithresh) THEN
             kz1=k1-1
          ELSE IF (k1 .GT. nc-ithresh) THEN
             kz1=k1-1-nc
          ELSE
             CYCLE
          ENDIF
          DO j1=nmin,nc
             !print*,'j1',j1
             IF (j1 .LT. ithresh) THEN
		ky1=j1-1
             ELSE IF (j1 .GT. nc-ithresh) THEN!j2 until june 6th
		ky1=j1-1-nc
             ELSE
		CYCLE
             ENDIF
             DO i1=1,ncell+2,2
                kx1=(i1-1)/2
		kr1=SQRT(kx1**2.0+ky1**2.0+kz1**2.0)*2.0*pi/box
		IF ((kr1>=kBinB(w1)) .AND.(kr1<=kBinB(w1+1))) THEN
                   deltaa(i1,j1,k1)=delta(i1,j1,k1)
                   deltaa(i1+1,j1,k1)=delta(i1+1,j1,k1)
                   deltaad(i1,j1,k1)=1.0d0
                   deltaad(i1+1,j1,k1)=0.0d0
		ENDIF
             ENDDO
          ENDDO
       ENDDO
       CALL fft3d(deltaa,deltaa,'e')
       CALL fft3d(deltaad,deltaad,'e')



       DO w2=1,w1!NRadBins
          PRINT*,'w2',w2
          ALLOCATE(deltab(Nc+2,Nc,Nc))
          deltab=0.0d0
          ALLOCATE(deltabd(Nc+2,Nc,Nc))
          deltabd=0.0d0
          DO k2=nmin,nc
             IF (k2 .LT. ithresh) THEN
                kz2=k2-1
             ELSE IF (k2 .GT. nc-ithresh) THEN
                kz2=k2-1-nc
             ELSE
                CYCLE
             ENDIF
             DO j2=nmin,nc
                IF (j2 .LT. ithresh) THEN
                   ky2=j2-1
                ELSE IF (j2 .GT. nc-ithresh) THEN
                   ky2=j2-1-nc
                ELSE
                   CYCLE
                ENDIF
                DO i2=1,ncell+2,2
                   kx2=(i2-1)/2


                   kr2=SQRT(kx2**2.0+ky2**2.0+kz2**2.0)*2.0*pi/box
                   IF ((kr2>=kBinB(w2)) .AND.(kr2<=kBinB(w2+1))) THEN
                      deltab(i2,j2,k2)=delta(i2,j2,k2)
                      deltab(i2+1,j2,k2)=delta(i2+1,j2,k2)
                      deltabd(i2,j2,k2)=1.0d0
                      deltabd(i2+1,j2,k2)=0.0d0
                   ENDIF
		ENDDO
             ENDDO
          ENDDO
          CALL fft3d(deltab,deltab,'e')
          CALL fft3d(deltabd,deltabd,'e')


          DO w3=1,w2!NRadBins
             IF (kbinc(w3)<=(kbinc(w1)+kbinc(w2)) .AND. kbinc(w3)>=ABS(kbinc(w1)-kbinc(w2))) THEN
                PRINT*,'w1,w2,w3',w1,w2,w3
                ALLOCATE(deltac(Nc+2,Nc,Nc))
                deltac=0.0d0
                ALLOCATE(deltacd(Nc+2,Nc,Nc))
                deltacd=0.0d0

                DO k3=nmin,nc
                   IF (k3 .LT. ithresh) THEN
                      kz3=k3-1
                   ELSE IF (k3 .GT. nc-ithresh) THEN
                      kz3=k3-1-nc
                   ELSE
                      CYCLE
                   ENDIF
                   DO j3=nmin,nc
                      IF (j3 .LT. ithresh) THEN
                         ky3=j3-1
                      ELSE IF (j3 .GT. nc-ithresh) THEN
                         ky3=j3-1-nc
                      ELSE
                         CYCLE
                      ENDIF
                      DO i3=1,ncell+2,2
                         kx3=(i3-1)/2

                         kr3=SQRT(kx3**2.0+ky3**2.0+kz3**2.0)*2.0*pi/box
                         IF ((kr3>=kBinB(w3)) .AND.(kr3<=kBinB(w3+1))) THEN
                            deltac(i3,j3,k3)=delta(i3,j3,k3)
                            deltac(i3+1,j3,k3)=delta(i3+1,j3,k3)
                            deltacd(i3,j3,k3)=1.0d0
                            deltacd(i3+1,j3,k3)=0.0d0
                         ENDIF
                      ENDDO
                   ENDDO
		ENDDO
		CALL fft3d(deltac,deltac,'e')
		CALL fft3d(deltacd,deltacd,'e')

                PRINT*,'Done ffts'
                !$OMP PARALLEL DO SHARED(nc) PRIVATE(k1,j1,i1)  NUM_THREADS(16) SCHEDULE(DYNAMIC,1) &
                !$OMP REDUCTION(+:RadCont,RadContD)
                DO j1=1,nc
                   DO k1=1,nc
                      DO i1=1,nc
                         RadCont(w1,w2,w3)=RadCont(w1,w2,w3)+deltaa(i1,j1,k1)*deltab(i1,j1,k1)*deltac(i1,j1,k1)
                         RadContD(w1,w2,w3)=RadContD(w1,w2,w3)+deltaad(i1,j1,k1)*deltabd(i1,j1,k1)*deltacd(i1,j1,k1)
                      ENDDO
                   ENDDO
                ENDDO
                !$OMP END PARALLEL DO

                IF (RadContD(w1,w2,w3)>0.0d0) THEN
                   RadCont(w1,w2,w3)=RadCont(w1,w2,w3)/RadContD(w1,w2,w3)
                ELSE
                   RadCont(w1,w2,w3)=0.0d0
                ENDIF


                PRINT*,'Done sum'

                DEALLOCATE(deltac)
                DEALLOCATE(deltacd)


             ENDIF
          ENDDO
          DEALLOCATE(deltab)
          DEALLOCATE(deltabd)
       ENDDO
       DEALLOCATE(deltaa)
       DEALLOCATE(deltaad)
    ENDDO


    CALL CPU_TIME(time2)
    time2=(time2-time1)
    WRITE(*,'(a,f8.2)') ' Bispectrum calculated in', time2
    RETURN
  END SUBROUTINE bispectrum_fft_kkk

  !///////////////////////////////////////////////////////////////////////////////
  !_________________________________________FFT __________________________________
  !///////////////////////////////////////////////////////////////////////////////
  SUBROUTINE fft3d(a,b,c)
    IMPLICIT NONE

    INCLUDE 'fftw3.f'
    REAL(8), DIMENSION(:,:,:):: a,b
    INTEGER(8),SAVE :: plan,iplan
    LOGICAL :: first_fft

    CHARACTER c
    REAL :: time1,time2

    CALL CPU_TIME(time1)



    !create plan
    DATA first_fft /.TRUE./
    IF (first_fft) THEN
       first_fft=.FALSE.
       PRINT*,'creating plan'
       CALL dfftw_init_threads
       CALL dfftw_plan_with_nthreads(nthrfftw)
       IF(ffttype=='mes') THEN
          WRITE(*,*) 'Measure'
          CALL dfftw_plan_dft_r2c_3d(plan, ncell, ncell, ncell, a, a,FFTW_MEASURE)
          CALL dfftw_plan_dft_c2r_3d(iplan, ncell, ncell, ncell, a, a,FFTW_MEASURE)
       ELSE
          WRITE(*,*) 'Estimate'
          CALL dfftw_plan_dft_r2c_3d(plan, ncell, ncell, ncell, a, a,FFTW_ESTIMATE)
          CALL dfftw_plan_dft_c2r_3d(iplan, ncell, ncell, ncell, a, a,FFTW_ESTIMATE)
       ENDIF
    ENDIF


    IF (c .EQ. 'f') THEN
       WRITE(*,*) 'Forward FFT'
       CALL dfftw_execute_dft_r2c(plan,a,a)
       a=a/REAL(NCell)**(3.)*box**3.0d0
    ELSE
       WRITE(*,*) 'Inverse FFT'
       CALL dfftw_execute_dft_c2r(iplan,a,a)
       a=a/box**3.0d0
    ENDIF


    CALL CPU_TIME(time2)
    time2=(time2-time1)
    WRITE(*,*) 'FFT Calculated in',time2
    RETURN
  END SUBROUTINE fft3d


  SUBROUTINE bispecthdf5export(filename,dsetname,bcnt,b,kavg)
    USE hdf5
    IMPLICIT NONE
    INTEGER :: error
    CHARACTER(*) :: filename ! File name
    CHARACTER(*) :: dsetname    ! Dataset name

    INTEGER(HID_T) :: file_id	! File identifier
    INTEGER(HID_T) :: dset_id	! Dataset identifier
    INTEGER(HID_T) :: dspace_id	! Dataspace identifier
    REAL(8), DIMENSION(nkbins,nkbins,nkbins) :: b
    REAL(8), DIMENSION(nkbins,nkbins,nkbins,3) :: kavg
    INTEGER, DIMENSION(nkbins,nkbins,nkbins) :: bcnt
    INTEGER(HSIZE_T), DIMENSION(3), PARAMETER :: dimsb=(/nkbins,nkbins,nkbins/)! Dataset dimensions
    INTEGER     ::   rankb = 3
    INTEGER(HSIZE_T), DIMENSION(4), PARAMETER :: dimsbk=(/nkbins,nkbins,nkbins,3/)! Dataset dimensions
    INTEGER     ::   rankbk = 4
    INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: dimsk=(/nkbins/)! Dataset dimensions
    INTEGER     ::   rankk = 1



    CALL h5open_f(error)
    CALL h5fcreate_f(TRIM(filename), H5F_ACC_TRUNC_F, file_id, error)

    CALL h5screate_simple_f(rankb, dimsb, dspace_id, error)
    CALL h5dcreate_f(file_id, TRIM(dsetname), H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, b, dimsb, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankb, dimsb, dspace_id, error)
    CALL h5dcreate_f(file_id, TRIM(dsetname)//'_cnt', H5T_NATIVE_INTEGER, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, bcnt, dimsb, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankbk, dimsbk, dspace_id, error)
    CALL h5dcreate_f(file_id, TRIM(dsetname)//'_kavg', H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, kavg, dimsbk, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankk, dimsk, dspace_id, error)
    CALL h5dcreate_f(file_id,'kbinc', H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, kbinc, dimsk, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)



    CALL h5fclose_f(file_id, error)
    CALL h5close_f(error)
  END SUBROUTINE bispecthdf5export

  SUBROUTINE bispectmuhdf5export(filename,dsetname,bcnt,b,kavg)
    USE hdf5
    IMPLICIT NONE
    INTEGER :: error
    CHARACTER(*) :: filename ! File name
    CHARACTER(*) :: dsetname    ! Dataset name

    INTEGER(HID_T) :: file_id	! File identifier
    INTEGER(HID_T) :: dset_id	! Dataset identifier
    INTEGER(HID_T) :: dspace_id	! Dataspace identifier
    REAL(8), DIMENSION(nkbins,nkbins,nmubins) :: b
    REAL(8), DIMENSION(nkbins,nkbins,nmubins,3) :: kavg
    INTEGER, DIMENSION(nkbins,nkbins,nmubins) :: bcnt
    INTEGER(HSIZE_T), DIMENSION(3), PARAMETER :: dimsb=(/nkbins,nkbins,nmubins/)! Dataset dimensions
    INTEGER     ::   rankb = 3
    INTEGER(HSIZE_T), DIMENSION(4), PARAMETER :: dimsbk=(/nkbins,nkbins,nmubins,3/)! Dataset dimensions
    INTEGER     ::   rankbk = 4
    INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: dimsk=(/nkbins/)! Dataset dimensions
    INTEGER     ::   rankk = 1



    CALL h5open_f(error)
    CALL h5fcreate_f(TRIM(filename), H5F_ACC_TRUNC_F, file_id, error)

    CALL h5screate_simple_f(rankb, dimsb, dspace_id, error)
    CALL h5dcreate_f(file_id, TRIM(dsetname), H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, b, dimsb, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankb, dimsb, dspace_id, error)
    CALL h5dcreate_f(file_id, TRIM(dsetname)//'_cnt', H5T_NATIVE_INTEGER, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, bcnt, dimsb, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankbk, dimsbk, dspace_id, error)
    CALL h5dcreate_f(file_id, TRIM(dsetname)//'_kavg', H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, kavg, dimsbk, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankk, dimsk, dspace_id, error)
    CALL h5dcreate_f(file_id,'kbinc', H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, kbinc, dimsk, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)



    CALL h5fclose_f(file_id, error)
    CALL h5close_f(error)
  END SUBROUTINE bispectmuhdf5export



  !==================================================================================================
  !w
  !====================================================================================================
  SUBROUTINE estimatorhdf5export(filename,dsetname,kavg,kcnts,linearpower, powermatrix)
    USE hdf5
    IMPLICIT NONE
    INTEGER :: error, rnk
    CHARACTER(*) :: filename ! File name
    CHARACTER(*) :: dsetname    ! Dataset name

    INTEGER(HID_T) :: file_id	! File identifier
    INTEGER(HID_T) :: dset_id	! Dataset identifier
    INTEGER(HID_T) :: dspace_id	! Dataspace identifier
    REAL(8), DIMENSION(nkbins,3,3,6) :: powermatrix
    REAL(8), DIMENSION(nkbins) :: linearpower
    REAL(8), DIMENSION(nkbins) :: kavg
    INTEGER, DIMENSION(nkbins) :: kcnts

    INTEGER(HSIZE_T), DIMENSION(4), PARAMETER :: dimsb=(/nkbins,3,3,6/)! Dataset dimensions
    INTEGER     ::   rankb = 4
    INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: dimsbk=(/nkbins/)! Dataset dimensions
    INTEGER     ::   rankbk = 1
    INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: dimsk=(/nkbins/)! Dataset dimensions
    INTEGER     ::   rankk = 1
    INTEGER(HSIZE_T), DIMENSION(2), PARAMETER :: dimsp=(/nkbins,6/)! Dataset dimensions
    INTEGER     ::   rankp = 2



    CALL h5open_f(error)
    CALL h5fcreate_f(TRIM(filename), H5F_ACC_TRUNC_F, file_id, error)

    CALL h5screate_simple_f(rankb, dimsb, dspace_id, error)
    CALL h5dcreate_f(file_id, TRIM(dsetname), H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, powermatrix, dimsb, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankbk, dimsbk, dspace_id, error)
    CALL h5dcreate_f(file_id,'kbin', H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, kavg, dimsbk, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankk, dimsk, dspace_id, error)
    CALL h5dcreate_f(file_id,'kcnt', H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, kcnts, dimsk, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankp, dimsp, dspace_id, error)
    CALL h5dcreate_f(file_id,'power', H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, linearpower, dimsp, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)



    CALL h5fclose_f(file_id, error)
    CALL h5close_f(error)
  END SUBROUTINE estimatorhdf5export


  !==================================================================================================
  !w
  !====================================================================================================
  SUBROUTINE ESTAVGhdf5export(filename,dsetname,kavg,kcnt,linearpower, powermatrix)
    USE hdf5
    IMPLICIT NONE
    INTEGER :: error
    CHARACTER(*) :: filename ! File name
    CHARACTER(*) :: dsetname    ! Dataset name

    INTEGER(HID_T) :: file_id	! File identifier
    INTEGER(HID_T) :: dset_id	! Dataset identifier
    INTEGER(HID_T) :: dspace_id	! Dataspace identifier
    REAL(8), DIMENSION(nkbins,3,5) :: powermatrix
    REAL(8), DIMENSION(nkbins) :: linearpower
    REAL(8), DIMENSION(nkbins) :: kavg
    INTEGER, DIMENSION(nkbins) :: kcnt
    INTEGER(HSIZE_T), DIMENSION(3), PARAMETER :: dimsb=(/nkbins,3,5/)! Dataset dimensions
    INTEGER     ::   rankb = 3
    INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: dimsbk=(/nkbins/)! Dataset dimensions
    INTEGER     ::   rankbk = 1
    INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: dimsk=(/nkbins/)! Dataset dimensions
    INTEGER     ::   rankk = 1
    INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: dimsp=(/nkbins/)! Dataset dimensions
    INTEGER     ::   rankp = 1



    CALL h5open_f(error)
    CALL h5fcreate_f(TRIM(filename), H5F_ACC_TRUNC_F, file_id, error)

    CALL h5screate_simple_f(rankb, dimsb, dspace_id, error)
    CALL h5dcreate_f(file_id, TRIM(dsetname), H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, powermatrix, dimsb, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankbk, dimsbk, dspace_id, error)
    CALL h5dcreate_f(file_id,'kbin', H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, kavg, dimsbk, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankk, dimsk, dspace_id, error)
    CALL h5dcreate_f(file_id,'kcnt', H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, kcnt, dimsk, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)

    CALL h5screate_simple_f(rankp, dimsp, dspace_id, error)
    CALL h5dcreate_f(file_id,'linear_power', H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, linearpower, dimsp, error)
    CALL h5dclose_f(dset_id, error)
    CALL h5sclose_f(dspace_id, error)



    CALL h5fclose_f(file_id, error)
    CALL h5close_f(error)
  END SUBROUTINE ESTAVGhdf5export


!================================================================================
!===== 3D fields on the grid & spline power spectrum
!================================================================================

SUBROUTINE Gridhdf5export(filename,dsetname,gridpower, delta)
  USE hdf5
  IMPLICIT NONE
  INTEGER :: error
  CHARACTER(*) :: filename ! File name
  CHARACTER(*) :: dsetname    ! Dataset name

  INTEGER(HID_T) :: file_id	! File identifier
  INTEGER(HID_T) :: dset_id	! Dataset identifier
  INTEGER(HID_T) :: dspace_id	! Dataspace identifier
  REAL(8), DIMENSION(:,:,:) :: gridpower, delta
  INTEGER(HSIZE_T), DIMENSION(3), PARAMETER :: dimsb=(/258,256,256/)! Dataset dimensions
  INTEGER     ::   rankb = 3
  INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: dimsk=(/nkbins/)! Dataset dimensions
  INTEGER     ::   rankk = 1
  INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: dimsp=(/nkbins/)! Dataset dimensions
  INTEGER     ::   rankp = 1


  CALL h5open_f(error)
  CALL h5fcreate_f(TRIM(filename), H5F_ACC_TRUNC_F, file_id, error)

  CALL h5screate_simple_f(rankb, dimsb, dspace_id, error)
  CALL h5dcreate_f(file_id, TRIM(dsetname), H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
  CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, gridpower, dimsb, error)
  CALL h5dclose_f(dset_id, error)
  CALL h5sclose_f(dspace_id, error)

  CALL h5screate_simple_f(rankb , dimsb, dspace_id, error)
  CALL h5dcreate_f(file_id, TRIM('deltag'), H5T_NATIVE_DOUBLE, dspace_id,dset_id, error)
  CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, delta, dimsb, error)
  CALL h5dclose_f(dset_id, error)
  CALL h5sclose_f(dspace_id, error)




  CALL h5fclose_f(file_id, error)
  CALL h5close_f(error)


END SUBROUTINE Gridhdf5export


END MODULE eval
