module nrmods

contains

FUNCTION splint(xa,ya,y2a,x)
IMPLICIT NONE
REAL(4), DIMENSION(:), INTENT(IN) :: xa,ya,y2a
REAL(4), INTENT(IN) :: x
REAL(4) :: splint
INTEGER(4) :: khi,klo,n
REAL(4) :: a,b,h
n=size(xa)
klo=max(min(locate(xa,x),n-1),1)
khi=klo+1
h=xa(khi)-xa(klo)
if (h == 0.0) write(*,*) 'nrerror splint'
a=(xa(khi)-x)/h
b=(x-xa(klo))/h
splint=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.0
END FUNCTION splint


SUBROUTINE spline(x,y,yp1,ypn,y2)
IMPLICIT NONE
REAL(4), DIMENSION(:), INTENT(IN) :: x,y
REAL(4), INTENT(IN) :: yp1,ypn
REAL(4), DIMENSION(:), INTENT(OUT) :: y2
INTEGER(4) :: n
REAL(4), DIMENSION(size(x)) :: a,b,c,r
n=size(x)
c(1:n-1)=x(2:n)-x(1:n-1)
r(1:n-1)=6.0*((y(2:n)-y(1:n-1))/c(1:n-1))
r(2:n-1)=r(2:n-1)-r(1:n-2)
a(2:n-1)=c(1:n-2)
b(2:n-1)=2.0*(c(2:n-1)+a(2:n-1))
b(1)=1.0
b(n)=1.0
if (yp1 > 0.99e30) then
   r(1)=0.0
   c(1)=0.0
else
   r(1)=(3.0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
   c(1)=0.5
end if
if (ypn > 0.99e30) then
   r(n)=0.0
   a(n)=0.0
else
   r(n)=(-3.0/(x(n)-x(n-1)))*((y(n)-y(n-1))/(x(n)-x(n-1))-ypn)
   a(n)=0.5
end if
call tridag_par(a(2:n),b(1:n),c(1:n-1),r(1:n),y2(1:n))
END SUBROUTINE spline


SUBROUTINE tridag_ser(a,b,c,r,u)
IMPLICIT NONE
REAL(4), DIMENSION(:), INTENT(IN) :: a,b,c,r
REAL(4), DIMENSION(:), INTENT(OUT) :: u
REAL(4), DIMENSION(size(b)) :: gam
INTEGER(4) :: n,j
REAL(4) :: bet
n=size(a)+1
bet=b(1)
if (bet == 0.0) write(*,*) 'nrerror'
u(1)=r(1)/bet
do j=2,n
   gam(j)=c(j-1)/bet
   bet=b(j)-a(j-1)*gam(j)
   if (bet == 0.0) &
      write(*,*) 'nrerror'
   u(j)=(r(j)-a(j-1)*u(j-1))/bet
end do
do j=n-1,1,-1
   u(j)=u(j)-gam(j+1)*u(j+1)
end do
END SUBROUTINE tridag_ser

RECURSIVE SUBROUTINE tridag_par(a,b,c,r,u)
IMPLICIT NONE
REAL(4), DIMENSION(:), INTENT(IN) :: a,b,c,r
REAL(4), DIMENSION(:), INTENT(OUT) :: u
INTEGER(4), PARAMETER :: NPAR_TRIDAG=4
INTEGER(4) :: n,n2,nm,nx
REAL(4), DIMENSION(size(b)/2) :: y,q,piva
REAL(4), DIMENSION(size(b)/2-1) :: x,z
REAL(4), DIMENSION(size(a)/2) :: pivc
n=size(a)+1
if (n < NPAR_TRIDAG) then
   call tridag_ser(a,b,c,r,u)
else
   if (maxval(abs(b(1:n))) == 0.0) &
      write(*,*) 'nrerror tridag'
   n2=size(y)
   nm=size(pivc)
   nx=size(x)
   piva = a(1:n-1:2)/b(1:n-1:2)
   pivc = c(2:n-1:2)/b(3:n:2)
   y(1:nm) = b(2:n-1:2)-piva(1:nm)*c(1:n-2:2)-pivc*a(2:n-1:2)
   q(1:nm) = r(2:n-1:2)-piva(1:nm)*r(1:n-2:2)-pivc*r(3:n:2)
   if (nm < n2) then
      y(n2) = b(n)-piva(n2)*c(n-1)
      q(n2) = r(n)-piva(n2)*r(n-1)
   end if
   x = -piva(2:n2)*a(2:n-2:2)
   z = -pivc(1:nx)*c(3:n-1:2)
   call tridag_par(x,y,z,q,u(2:n:2))
   u(1) = (r(1)-c(1)*u(2))/b(1)
   u(3:n-1:2) = (r(3:n-1:2)-a(2:n-2:2)*u(2:n-2:2) &
      -c(3:n-1:2)*u(4:n:2))/b(3:n-1:2)
   if (nm == n2) u(n)=(r(n)-a(n-1)*u(n-1))/b(n)
end if
END SUBROUTINE tridag_par


FUNCTION locate(xx,x)
IMPLICIT NONE
REAL(4), DIMENSION(:), INTENT(IN) :: xx
REAL(4), INTENT(IN) :: x
INTEGER(4) :: locate
INTEGER(4) :: n,jl,jm,ju
LOGICAL :: ascnd
n=size(xx)
ascnd = (xx(n) >= xx(1))
jl=0
ju=n+1
do
   if (ju-jl <= 1) exit
   jm=(ju+jl)/2
   if (ascnd .eqv. (x >= xx(jm))) then
      jl=jm
   else
      ju=jm
   end if
end do
if (x == xx(1)) then
   locate=1
else if (x == xx(n)) then
   locate=n-1
else
   locate=jl
end if
END FUNCTION locate


end module nrmods
