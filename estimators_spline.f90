MODULE HaloTYPE

  TYPE Halo

     INTEGER  :: HN                       ! number of particles in the halo
     REAL(8) :: Hmass                    ! Halo Mass fof=0.2 [10**10 Mo/h]

     REAL(8), DIMENSION(3) :: HPos       ! centre of mass position [Mpc/h]
     REAL(8), DIMENSION(3) :: HVel       ! centre of mass velocity [km/s]

     REAL(8) :: Hrad                     ! radius of halo = mean of last 4 part.
     REAL(8) :: Hradhalf                 ! Half-mass radius
     REAL(8) :: HDisp1D                  ! 1D velocity dispersion [km/s]

     REAL(8), DIMENSION(6) :: HInert     ! Inertia matrix:
     ! I11, I22, I33, I12, I13, I23
  END TYPE Halo

END MODULE HaloTYPE

PROGRAM power
USE halotype
USE nrmods
USE eval           ! Frequently used Routines for Correlation and Power Spectrum
USE snapshot       ! Reads in Dark Matter Particles
IMPLICIT NONE


! All that in and out Files
CHARACTER*200 :: Folder
CHARACTER*200 :: InFileBase
CHARACTER*200 :: PsFileBase, PsOutFile,Extension , dmgaussFile
CHARACTER*200 :: datadir,HalFileBase,HalInFile,HaloFileBase,HaloOutFile, PowerFile
CHARACTER*10 :: nodestr,snapstr,ncstr

INTEGER :: idat                ! =1 read data; =0 get header
INTEGER :: iPos                ! =1 read positions; =0 do not.
INTEGER :: iVel                ! =1 read velocities; =0 do not.
INTEGER :: iID                 ! =1 read the ID's; =0 do not.
INTEGER :: iRed                ! =1 no redshift space distortions
                                 ! = (2,3,4) apply distortion in (x-,y-,z-)
TYPE(snapFILES) :: sF

! Switches
INTEGER :: doCorrect						! do CIC correction or not
INTEGER :: doBinnedH
INTEGER :: NodeNumber
INTEGER :: FileNumber
INTEGER :: NHalBin,iMBin,cnt,NHalTot


TYPE(halo) :: HaloFinal
INTEGER, PARAMETER :: NMassBins=5
INTEGER, PARAMETER :: MBinDim=6
REAL, DIMENSION(NMassBins+1) :: MBinB
INTEGER, DIMENSION(NMassBins+1) :: NBinB
REAL, DIMENSION(NMassBins) :: MBinC
INTEGER, DIMENSION(NMassBins) :: NHalosBin
REAL(4), DIMENSION(:), ALLOCATABLE :: HalM
REAL(4), DIMENSION(:,:), ALLOCATABLE :: HalPos,HalVel


! Box dimensions
REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltadm,deltag, deltahsquare, tidal, shift, deltag2
REAL(8), DIMENSION(:,:,:), ALLOCATABLE :: deltar, deltadmgauss,gridlinpower, deltadmsquare

INTEGER, DIMENSION(NkBins) :: kBinCnt
REAL(8), DIMENSION(NkBins) :: kTrueBinC
REAL(8), DIMENSION(NkBins) :: powerdfdf
REAL(8), DIMENSION(NkBins,MBinDim) :: powerdmdg,powerdgdg
REAL(8), DIMENSION(NkBins,MBinDim) :: powerAgD, powerAsD, powerAtD
REAL(8), DIMENSION(NkBins,MBinDim) :: powerAgAg, powerAsAs, powerAtAt
REAL(8), DIMENSION(NkBins,MBinDim) :: powerAgAs, powerAgAt, powerAsAt
REAL(8), DIMENSION(NkBins,3,3,MBinDim) :: powermatrix

REAL :: time1,time2
INTEGER :: OMP_GET_NUM_THREADS,TID,OMP_GET_THREAD_NUM

INTEGER :: i
REAL(4) :: hradius

REAL(8), DIMENSION(5) :: b1
REAL(8), DIMENSION(5) :: ShotNoise
REAL(8) :: growth

!growth=0.013134836482296224;
growth=0.01313483;

!powermatrix = 0.0d0

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CALL CPU_TIME(time1)
! Size of FFT Grid
NCell=256
box=1500.
! Redshift Output
FileNumber=9
! Realization
NodeNumber=1

CALL genbink(0.003d0,0.5d0,'log')

hradius = 4.0
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!				> > > FILENAMES < < <
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

IF (FileNumber<10) THEN
    WRITE(snapstr,'(a2,i1)') '00',FileNumber
ELSE
    WRITE(snapstr,'(a1,i2)') '0',FileNumber
ENDIF

!simulation
IF (NodeNumber<10) THEN
    WRITE(nodestr,'(i1)') NodeNumber
ELSE
    WRITE(nodestr,'(i2)') NodeNumber
ENDIF

CALL getarg(1,nodestr)

IF (Ncell<100) THEN
    WRITE(ncstr,'(i2)') Ncell
ELSEIF (Ncell<1000) THEN
    WRITE(ncstr,'(i3)') Ncell
ELSEIF (Ncell<10000) THEN
    WRITE(ncstr,'(i4)') Ncell
ENDIF

doCorrect=1
doBinnedH=1

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Output File Names
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Extension='NODE'//TRIM(nodestr)//'_'//ASSIGN//'C1_'//TRIM(ncstr)//'_'
Folder='/fast/space/projects/dp002/dc-abid1/reconstruction/Data/'


!PsFileBase='power_'
!PsOutFile=trim(Folder)//trim(PsFileBase)//trim(Extension)//trim(snapstr)//'.dat'
!write(*,*) 'Writing Power-Spectrum to ',PsOutFile


!Halo Number Mass Bins
NbinB=(/20,60,180,540,1620,4860/)

! linear bias: b1
b1 = (/1.06733, 1.32421, 1.82618, 2.69749, 4.20851/)
!shot noise
ShotNoise = (/1592.38, 4615.02, 15018.4, 60669.8, 403129.0/)

datadir='/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//TRIM(nodestr)//'/DATA/reducedGROUPS/'
HalInFile=TRIM(datadir)//'GRPS_CMS_wmap7_fid_run'//TRIM(nodestr)//'_'//TRIM(snapstr)


! initial Gaussian field
dmgaussFile = '/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//TRIM(nodestr)//'_ZELD/ICs/delta_256.dat'

PowerFile = 'wmap7_camb_matterpower_z0.dat'
!///////////////////////////////////////////////////////////////////////////////
!__________________________________DARK MATTER__________________________________
!///////////////////////////////////////////////////////////////////////////////
WRITE(*,'(a)') '\n\n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
WRITE(*,'(a)') '          > > > Loading initial Gaussian Field < < <'
WRITE(*,'(a)') ' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'


ALLOCATE(deltadmgauss(1:NCell+2,1:NCell,1:NCell))
ALLOCATE(gridlinpower(1:NCell+2,1:NCell,1:NCell))

deltadmgauss = 0.


! loading the initidal gaussian field - this field is already in Fourier transform so no need to do CIC correction.
OPEN(13,file=dmgaussFile,form='unformatted',status='old')
READ(13) deltadmgauss
CLOSE(13)

!CALL smdir(deltadmgauss,hradius,NCell)
!=============  Linear power spectrum : <delta, delta>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(deltadmgauss,deltadmgauss,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powerdfdf)
DEALLOCATE(deltar)

! calculating power spectrum on grid
CALL genPfield(gridlinpower, PowerFile)


!print*, 'shape of spline power', Shape(gridlinpower)

! appliying thre weiner filters
!deltadmgauss = deltadmgauss/gridlinpower

!CALL Gridhdf5export(TRIM(Folder)//'Spline/spline_power'//'_NODE'//TRIM(nodestr)//'_'//TRIM(ncstr)//'.h5','lin_power',gridlinpower,deltadmgauss)
!stop

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


!First FFT - Determines FFT strategy
!allocate(deltar(1:NCell+2,1:NCell,1:NCell))
!call fft3d(deltar,deltar,'f')
!deallocate(deltar)


!///////////////////////////////////////////////////////////////////////////////
!__________________________________DARK MATTER__________________________________
!///////////////////////////////////////////////////////////////////////////////

ALLOCATE(tidal(1:NCell+2,1:NCell,1:NCell))
ALLOCATE(shift(1:NCell+2,1:NCell,1:NCell))
ALLOCATE(deltahsquare(1:NCell+2,1:NCell,1:NCell))

tidal=0
shift =0
deltahsquare=0.



write(*,'(a)') '\n\n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
write(*,'(a)') '          > > > Loading dark matter < < <'
write(*,'(a)') ' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'

allocate(deltadm(1:NCell+2,1:NCell,1:NCell))

deltadm=0.

iDat=1; iPos=0; iVel=0; iID=0; iRed=0

! Fiducial Cosmology Final
sf%SNAPDATA = '/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//trim(nodestr)//'/DATA/'
sf%SNAPBASE = 'wmap7_fid_run'//trim(nodestr)//'_'
sf%SNAPEXT = snapstr
sf%SNAPNFILES = 24

call read_snap(sF,iDat,iPos,iVel,iID,iRed,NCell,deltadm)
call normalize(deltadm)

call fft3d(deltadm,deltadm,'f')

if (doCorrect==1) then
    call ciccorrect(deltadm,1.0d0)
endif

!deltadm=deltadmgauss

ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(deltadm,deltadm,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powerdgdg(:,6))
!
DEALLOCATE(deltar)

!----------------------------------------------------
ALLOCATE(deltag(1:NCell+2,1:NCell,1:NCell))
ALLOCATE(deltag2(1:NCell+2,1:NCell,1:NCell))
deltag=0.d0
deltag2=0.d0

!-----
CALL smdir(deltadm,hradius,NCell) ! smoothing of the halo density field
deltag2 = deltadm/(gridlinpower)  ! both are in FS
deltag = (deltadm*gridlinpower)/(gridlinpower)  ! both are in FS
!-----

!============= initializing shift and
shift = 0.0d0
tidal = 0.0d0
!========== shift
!CALL psigradrecon(deltag,shift) ! shift estimator from the halo field
CALL psigradrecon2p(deltag,deltag2,shift) ! shift estimator from the halo field
!========== tidal
!CALL S2recon(deltag,tidal) ! tidal estimator computed from the halo field
CALL S2recon2p(deltag,deltag2,tidal) ! tidal estimator computed from the halo field
tidal = (2.0d0/7.0d0)*tidal ! using simon's conventions
!========== deltahsquare
CALL fft3d(deltag,deltag,'e')
CALL fft3d(deltag2,deltag2,'e')
!CALL fft3d(deltadmgauss,deltadmgauss,'e')
deltahsquare = (5.0d0/7.0d0)*deltag*deltag2  ! square of the halo field
!deltahsquare = deltadmgauss*deltadmgauss  ! square of the halo field
CALL fft3d(deltahsquare,deltahsquare,'f')


CALL fft3d(deltag,deltag,'f')
CALL fft3d(deltag2,deltag2,'f')

!=============  <Ag, delta>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(deltadmgauss,deltahsquare,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,1,1,6))
DEALLOCATE(deltar)
!=============  <As, delta>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(deltadmgauss,shift,deltar)
! bin the shift field.
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,1,2,6))
DEALLOCATE(deltar)
!=============  <At, delta>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(deltadmgauss,tidal,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,1,3,6))
DEALLOCATE(deltar)


!=============  <Ag, Ag>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(deltahsquare,deltahsquare,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,2,1,6))
DEALLOCATE(deltar)
!=============  <As, As>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(shift,shift,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,2,2,6))
DEALLOCATE(deltar)
!=============  <At, At>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(tidal,tidal,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,2,3,6))
DEALLOCATE(deltar)

!=============  <Ag, As>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(deltahsquare,shift,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,3,1,6))
DEALLOCATE(deltar)
!=============  <Ag, At>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(deltahsquare,tidal,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,3,2,6))
DEALLOCATE(deltar)
!=============  <As, At>
ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
deltar=0.d0
CALL comprod(shift,tidal,deltar)
CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,3,3,6))
DEALLOCATE(deltar)

! deallocating delta halo field
DEALLOCATE(deltag)
DEALLOCATE(deltag2)
DEALLOCATE(deltadm)



!///////////////////////////////////////////////////////////////////////////////
!__________________________________Mass dependent power_________________________
!///////////////////////////////////////////////////////////////////////////////

	IF (doBinnedH==1) THEN


	DO iMBin=1,5

		WRITE(*,'(a)') '\n\n ***********************************************'
		WRITE(*,'(a)') '          > > > Halos Mass-Binned < < <'
		WRITE(*,'(a)') ' ***********************************************\n\n'

		OPEN(11,file=TRIM(HalInFile),status='old',form='unformatted')
		READ(11) NHalTot

		PRINT*,'NHalTot',NHalTot
		cnt=0
		DO i=1,NHalTot
			READ(11) HaloFINAL
			IF (HaloFINAL%HN>=NBinB(iMBin) .AND. HaloFINAL%HN<NBinB(iMBin+1)) THEN
					cnt=cnt+1
			ENDIF
		END DO
		CLOSE(11)
		NHalBin=cnt
		PRINT*,'NHalBin',NHalBin

		ALLOCATE(HalM(NHalBin))
		ALLOCATE(HalPos(3,NHalBin))
		ALLOCATE(HalVel(3,NHalBin))



		OPEN(11,file=TRIM(HalInFile),status='old',form='unformatted')
		READ(11) NHalTot
		cnt=0
		DO i=1,NHalTot
			READ(11) HaloFINAL
			IF (HaloFINAL%HN>=NBinB(iMBin) .AND. HaloFINAL%HN<NBinB(iMBin+1)) THEN
			    cnt=cnt+1
			    HalM(cnt)=HaloFINAL%HMass
			    HalPos(:,cnt)=HaloFINAL%HPos(:)
			    HalVel(:,cnt)=HaloFINAL%HVel(:)/SQRT(0.0d0+1.0d0)
			ENDIF
		END DO
		CLOSE(11)

		PRINT*,'Haloes Loaded'

			ALLOCATE(deltag(1:NCell+2,1:NCell,1:NCell))
      ALLOCATE(deltag2(1:NCell+2,1:NCell,1:NCell))
			deltag=0.d0
      deltag2=0.d0

			CALL cicmass(NHalBin,HalPos,deltag)
			MBinC(iMBin)=SUM(HalM)/REAL(NHalBin)

			CALL normalize(deltag)

			CALL fft3d(deltag,deltag,'f')

			IF (doCorrect==1) THEN
 				CALL ciccorrect(deltag,1.0d0)
			ENDIF

      ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
      deltar=0.d0
      CALL comprod(deltag,deltag,deltar)
      CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powerdgdg(:,iMBin))
      DEALLOCATE(deltar)


      !-----
      !ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
      !deltag = deltadmgauss
      !CALL fft3d(deltadmgauss,deltadmgauss,'e')
      !deltag = deltadmgauss*deltadmgauss  ! checking with deltah = deltadmsquare
      !CALL fft3d(deltadmgauss,deltadmgauss,'f')
      !CALL fft3d(deltag,deltag,'e')
      !DEALLOCATE(deltadmsquare)

			CALL smdir(deltag,hradius,NCell) ! smoothing of the halo density field
      deltag2 = deltag/(b1(iMBin)**2.0*gridlinpower  + ShotNoise(iMBin))  ! both are in FS
      deltag = (deltag*gridlinpower)/(b1(iMBin)**2.0*gridlinpower  + ShotNoise(iMBin))  ! both are in FS
      !deltag2=deltag/gridlinpower
      !-----

		  !============= initializing shift and
		  shift = 0.0d0
		  tidal = 0.0d0
		  !========== shift
		  !CALL psigradrecon(deltag,shift) ! shift estimator from the halo field
      CALL psigradrecon2p(deltag,deltag2,shift) ! shift estimator from the halo field
		  !========== tidal
		  !CALL S2recon(deltag,tidal) ! tidal estimator computed from the halo field
      CALL S2recon2p(deltag,deltag2,tidal) ! tidal estimator computed from the halo field
      tidal = (2.0d0/7.0d0)*tidal ! using simon's conventions
		  !========== deltahsquare
		  CALL fft3d(deltag,deltag,'e')
      CALL fft3d(deltag2,deltag2,'e')
      !CALL fft3d(deltadmgauss,deltadmgauss,'e')
			deltahsquare = (5.0d0/7.0d0)*deltag*deltag2  ! square of the halo field
      !deltahsquare = deltadmgauss*deltadmgauss  ! square of the halo field
			CALL fft3d(deltahsquare,deltahsquare,'f')


		  CALL fft3d(deltag,deltag,'f')
      CALL fft3d(deltag2,deltag2,'f')

      !=============  <Ag, delta>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.d0
			CALL comprod(deltadmgauss,deltahsquare,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,1,1,iMBin))
			DEALLOCATE(deltar)
			!=============  <As, delta>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.d0
			CALL comprod(deltadmgauss,shift,deltar)
      ! bin the shift field.
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,1,2,iMBin))
			DEALLOCATE(deltar)
			!=============  <At, delta>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.d0
			CALL comprod(deltadmgauss,tidal,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,1,3,iMBin))
			DEALLOCATE(deltar)


			!=============  <Ag, Ag>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.d0
			CALL comprod(deltahsquare,deltahsquare,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,2,1,iMBin))
			DEALLOCATE(deltar)
			!=============  <As, As>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.d0
			CALL comprod(shift,shift,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,2,2,iMBin))
			DEALLOCATE(deltar)
			!=============  <At, At>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.d0
			CALL comprod(tidal,tidal,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,2,3,iMBin))
			DEALLOCATE(deltar)

			!=============  <Ag, As>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.d0
			CALL comprod(deltahsquare,shift,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,3,1,iMBin))
			DEALLOCATE(deltar)
			!=============  <Ag, At>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.d0
			CALL comprod(deltahsquare,tidal,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,3,2,iMBin))
			DEALLOCATE(deltar)
			!=============  <As, At>
			ALLOCATE(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.d0
			CALL comprod(shift,tidal,deltar)
 			CALL powerspectrum(deltar,kBinCnt,kTrueBinC,powermatrix(:,3,3,iMBin))
			DEALLOCATE(deltar)

			! deallocating delta halo field
			DEALLOCATE(deltag)
      DEALLOCATE(deltag2)


			DEALLOCATE(HalM)
			DEALLOCATE(HalVel)
			DEALLOCATE(HalPos)

		ENDDO
	ENDIF

CALL estimatorhdf5export(TRIM(Folder)//'correlations_Wiener/Estimators_spline_CorrMatrix'//'_NODE'//TRIM(nodestr)//'_'//TRIM(ncstr)//'_Rh4.h5','Correlations',kbinc, kbincnt,powerdgdg, powermatrix)

PRINT*,'kbins',NkBins

	!if (doBinnedH==1) then


	!open(20,file=trim(HaloOutFile),form='formatted',status='replace')
 	!do i=1,NkBins
		!write(20,'(2ES30.10,1I30,11ES30.10)') kbinc(i),ktruebinc(i),kbincnt(i),powerdfdf(i), powerdmdg(i,:), powerdgdg(i,:)
   	!enddo
   	!close(20)

 	!endif


DEALLOCATE(gridlinpower)
DEALLOCATE(deltadmgauss)
DEALLOCATE(deltahsquare)
DEALLOCATE(tidal)
DEALLOCATE(shift)

CALL CPU_TIME(time2)
time2=(time2-time1)
WRITE(*,'(a,f8.2)') 'All done in', time2


END PROGRAM power
